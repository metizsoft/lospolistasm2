<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Bss\SizeChart\Model\ResourceModel;

/**
 * Class UpgradeData
 *
 * @package Bss\SizeChart\Setup
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * Model Size Chart
     *
     * @var \Bss\SizeChart\Model\SizeChart $modelSizeChart
     */
    protected $modelSizeChart;

    /**
     * Eav Attribute
     *
     * @var Attribute $eavAttribute
     */
    protected $eavAttribute;

    /**
     * ResourceModel SizeChart
     *
     * @var ResourceModel\SizeChart $resourceSizeChart
     */
    protected $resourceSizeChart;

    /**
     * UpgradeData constructor.
     *
     * @param \Bss\SizeChart\Model\SizeChart $modelSizeChart
     * @param Attribute $eavAttribute
     * @param ResourceModel\SizeChart $resourceSizeChart
     */
    public function __construct(
        \Bss\SizeChart\Model\SizeChart $modelSizeChart,
        Attribute $eavAttribute,
        ResourceModel\SizeChart $resourceSizeChart
    ) {
        $this->modelSizeChart = $modelSizeChart;
        $this->eavAttribute = $eavAttribute;
        $this->resourceSizeChart = $resourceSizeChart;
    }

    /**
     * Get Attribute Id
     *
     * @return int
     */
    protected function getAttributeId()
    {
        return $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
    }

    /**
     * Upgrade
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Statement_Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $tableRule = 'bss_rule';
            $tableSizeChartRule = 'bss_sc_rule';
            $collectionSizeChart = $this->modelSizeChart->getCollection();
            $codeAttributeSizeChart = $this->getAttributeId();
            foreach ($collectionSizeChart as $sizeChart) {
                $sizeChartId = $sizeChart->getData('size_chart_value_id');
                $storeId = $sizeChart->getIdStore();
                $arrSku = $this->resourceSizeChart->getArraySkuBySizeChartStore(
                    $storeId,
                    $sizeChartId,
                    $codeAttributeSizeChart
                );
                 $stringSku = implode(', ', $arrSku);
                 $condition = '{"type":"Magento\\\CatalogRule\\\Model\\\Rule\\\Condition\\\Combine","' .
                     'attribute":null,"operator":null,"value":"1","is_value_processed":null,"' .
                     'aggregator":"all","conditions":[{"' .
                     'type":"Magento\\\CatalogRule\\\Model\\\Rule\\\Condition\\\Product","' .
                     'attribute":"sku","operator":"()","value":"' .
                     $stringSku . '","is_value_processed":false}]}';
                 $this->resourceSizeChart->upgradeDataRule($tableRule, $condition);
                 $ruleId = $this->resourceSizeChart->getLastIdRule();
                 $this->resourceSizeChart->upgradeData($tableSizeChartRule, $ruleId, $sizeChartId);
            }
        }

        $installer->endSetup();
    }
}