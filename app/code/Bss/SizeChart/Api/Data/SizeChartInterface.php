<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Api\Data;

/**
 * Interface SizeChartInterface
 *
 * @package Bss\SizeChart\Api\Data
 */
interface SizeChartInterface
{
    /**
     * Get Id Size Chart
     *
     * @return int
     */
    public function getId();

    /**
     * Set Id Size Chart
     *
     * @param int $id
     * @return int
     */
    public function setId($id);

    /**
     * Get Title Size Chart
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set Title Size Chart
     *
     * @param string $title
     * @return string
     */
    public function setTitle($title);

    /**
     * Get Content Size Chart
     *
     * @return string
     */
    public function getContent();

    /**
     * Set content Size Chart
     *
     * @param string $content
     * @return string
     */
    public function setContent($content);
}
