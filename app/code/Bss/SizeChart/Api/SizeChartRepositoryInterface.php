<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Api;

/**
 * Interface SizeChartRepositoryInterface
 *
 * @package Bss\SizeChart\Model\Api
 */
interface SizeChartRepositoryInterface
{
    /**
     * Get Size Chart Product Id By Store
     *
     * @param int $idProduct
     * @param int $idStore
     * @return array
     */
    public function getSizeChartProductIdByStore($idProduct, $idStore);

    /**
     * Get all size chart by store view
     *
     * @param int $productId
     * @param int $storeId
     * @return \Bss\SizeChart\Model\SizeChart
     */
    public function getAllSizeChartByStoreView($productId, $storeId);
}
