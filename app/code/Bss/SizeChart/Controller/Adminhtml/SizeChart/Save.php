<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Controller\Adminhtml\SizeChart;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResourceConnection;
use Bss\SizeChart\Model\SizeChartRepository;
use Bss\SizeChart\Model\ResourceModel\SizeChart;

/**
 * Class Save
 *
 * @package Bss\SizeChart\Controller\Adminhtml\SizeChart
 */
class Save extends Action
{

    /**
     * const
     */
    const ADMIN_RESOURCE = 'Bss_SizeChart::save';

    /**
     * ResourceConnection
     *
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * SizeChart
     *
     * @var SizeChart
     */
    protected $resourceSizeChart;

    /**
     * Resource Rule
     * @var \Bss\SizeChart\Model\ResourceModel\Rule
     */
    protected $resourceRule;

    /**
     * Rule Factory
     *
     * @var \Bss\SizeChart\Model\RuleFactory
     */
    protected $ruleFactory;

    /**
     * Helper Data
     *
     * @var \Bss\SizeChart\Helper\Data
     */
    protected $helper;

    /**
     * SizeChartRepository
     *
     * @var SizeChartRepository $sizeChartRepository
     */
    protected $sizeChartRepository;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param ResourceConnection $resource
     * @param SizeChart $resourceSizeChart
     * @param \Bss\SizeChart\Model\ResourceModel\Rule $resourceRule
     * @param \Bss\SizeChart\Model\RuleFactory $ruleFactory
     * @param \Bss\SizeChart\Helper\Data $helper
     * @param SizeChartRepository $sizeChartRepository
     * @return void
     */
    public function __construct(
        Action\Context $context,
        ResourceConnection $resource,
        SizeChart $resourceSizeChart,
        \Bss\SizeChart\Model\ResourceModel\Rule $resourceRule,
        \Bss\SizeChart\Model\RuleFactory $ruleFactory,
        \Bss\SizeChart\Helper\Data $helper,
        SizeChartRepository $sizeChartRepository
    ) {
        parent::__construct($context);
        $this->resource = $resource;
        $this->resourceSizeChart = $resourceSizeChart;
        $this->resourceRule = $resourceRule;
        $this->ruleFactory = $ruleFactory;
        $this->helper = $helper;
        $this->sizeChartRepository = $sizeChartRepository;
    }

    /**
     * Get default display
     *
     * @return int
     */
    protected function getDefaultDisplay()
    {
        $displayDefault = $this->helper->getDefaultDisplay();
        $displayDefaultReturn = isset($displayDefault) ? $displayDefault : 1;
        return $displayDefaultReturn;
    }

    /**
     * Get Array product Id
     *
     * @param int $arrProductId
     * @return int array
     */
    protected function deleteOnArray($arrProductId)
    {
        if (end($arrProductId) == "on") {
            array_pop($arrProductId);
            return $arrProductId;
        } else {
            return $arrProductId;
        }
    }

    /**
     * Convert String to Array
     *
     * @param string $string
     * @return array
     */
    protected function stringToArrayProductId($string)
    {
        $arrProductId = explode("&", $string);
        $this->deleteOnArray($arrProductId);
        return $arrProductId;
    }

    /**
     * Get Array Product By Size Chart
     *
     * @param int $idSizeChart
     * @return array
     */
    protected function getArrProductBySizeChart($idSizeChart)
    {
        $attributeId = $this->helper->getAttributeSizeChartId();
        $arrProductId = $this->resourceSizeChart->getArrProductBySizeChart($idSizeChart, $attributeId);
        $arrProductIdChance = [];
        foreach ($arrProductId as $productId) {
            array_push($arrProductIdChance, $productId["entity_id"]);
        }
        return $arrProductIdChance;
    }

    /**
     * Get Array Product
     *
     * @param array $arrayFirst
     * @param array $arraySecond
     * @return array
     */
    protected function getProducts($arrayFirst = [], $arraySecond = [])
    {
        $productsSelected = array_diff($arrayFirst, $arraySecond);
        return $productsSelected;
    }

    /**
     * Get Size Chart By Id
     *
     * @param int $idSizeChart
     * @return \Bss\SizeChart\Model\SizeChart
     */
    protected function getSizeChartById($idSizeChart)
    {
        $sizeChart = $this->sizeChartRepository->getById($idSizeChart);
        return $sizeChart;
    }

    /**
     * Update Products
     *
     * @param array $arrayProductsId
     * @param int $sizeChartId
     * @param int $overwrite
     * @param int $storeId
     * @return void
     */
    protected function updateProducts($arrayProductsId, $sizeChartId, $overwrite, $storeId)
    {
        $attributeId = $this->helper->getAttributeSizeChartId();
        foreach ($arrayProductsId as $productId) {
            $connection = $this->resource->getConnection();
            $table = $connection->getTableName('catalog_product_entity_varchar');
            $sql = $this->resourceSizeChart->buildSql($table, $productId, $attributeId, $storeId);
            $result = $connection->fetchRow($sql);
            if ($result != null) {
                if ($sizeChartId != "0") {
                    $sizeChart = $this->getSizeChartById($result);
                    $sizeChartAfter = $this->getSizeChartById($sizeChartId);
                    if ($overwrite == 0) {
                        if ($sizeChart->getPriority() <= $sizeChartAfter->getPriority() && isset($sizeChartAfter)) {
                            $this->addDataInDatabase($productId, $sizeChartId, $storeId);
                        }
                    } else {
                        $this->addDataInDatabase($productId, $sizeChartId, $storeId);
                    }
                } else {
                    $this->addDataInDatabase($productId, $sizeChartId, $storeId);
                }
            } else {
                $this->addDataInDatabase($productId, $sizeChartId, $storeId);
            }
        }
    }

    /**
     * Add data in database
     *
     * @param int $productId
     * @param int $sizeChartId
     * @param int $storeId
     * @return void
     */
    protected function addDataInDatabase($productId, $sizeChartId, $storeId)
    {
        if ($productId != 0) {
            if ($this->checkProductIssetSizeChart($productId, $storeId)) {
                $this->updateProduct($productId, $sizeChartId, $storeId);
            } else {
                $this->insertProduct($productId, $sizeChartId, $storeId);
            }
        }
    }

    /**
     * Check Product Isset Size Chart
     *
     * @param int $productId
     * @param int $storeId
     * @return bool
     */
    protected function checkProductIssetSizeChart($productId, $storeId)
    {
        $check = true;
        $attributeId = $this->helper->getAttributeSizeChartId();
        $connection = $this->resource->getConnection();
        $table = $connection->getTableName('catalog_product_entity_varchar');
        $sql = $this->resourceSizeChart->buildSql($table, $productId, $attributeId, $storeId);
        $result = $connection->fetchRow($sql);
        if (!$result) {
            $check = false;
        }
        return $check;
    }

    /**
     * Update Product
     *
     * @param int $productId
     * @param int $sizeChartId
     * @param int $storeId
     * @return void
     */
    protected function updateProduct($productId, $sizeChartId, $storeId)
    {
        $attributeId = $this->helper->getAttributeSizeChartId();
        $connection = $this->resource->getConnection();
        $table = $connection->getTableName('catalog_product_entity_varchar');
        $bind = ['value' => (int)$sizeChartId];
        $where = [
            'entity_id = ?' => (int)$productId,
            'attribute_id = ?' => (int)$attributeId,
            'store_id = ?' => (int)$storeId
        ];
        $connection->update($table, $bind, $where);
    }

    /**
     * Insert Product
     *
     * @param int $productId
     * @param int $sizeChartId
     * @param int $storeId
     * @return void
     */
    protected function insertProduct($productId, $sizeChartId, $storeId)
    {
        $attributeId = $this->helper->getAttributeSizeChartId();
        $connection = $this->resource->getConnection();
        $table = $connection->getTableName('catalog_product_entity_varchar');
        $bind = [
            'value' => $sizeChartId,
            'entity_id' => (int)$productId,
            'attribute_id' => (int)$attributeId,
            'store_id' => (int)$storeId
        ];
        $this->resourceSizeChart->insertProduct($table, $bind);
    }

    /**
     * Treat String
     *
     * @param array $stringDataDisplay
     * @return string
     */
    protected function treatString($stringDataDisplay = [])
    {
        $stringDisplay = implode(",", $stringDataDisplay);
        return $stringDisplay;
    }

    /**
     * Delete DataBase Product Assign SizeChart
     *
     * @param string $table
     * @param array $where
     * @return void
     */
    protected function deleteDataBaseProductAssignSizeChart($table, $where)
    {
        $connection = $this->resource->getConnection();
        $connection->delete($table, $where);
    }

    /**
     * Reset Product Assign SizeChart
     *
     * @param array $arrProduct
     * @param int $idSizeChart
     * @return void
     */
    protected function resetProductAssignSizeChart($arrProduct, $idSizeChart)
    {
        $connection = $this->resource->getConnection();
        $table = $connection->getTableName('catalog_product_entity_varchar');
        foreach ($arrProduct as $idProduct) {
            $where = [
                "value = ?" => $idSizeChart,
                "entity_id = ?" => $idProduct
            ];
            $this->deleteDataBaseProductAssignSizeChart($table, $where);
        }
    }

    /**
     * Handle Data Rule
     *
     * @param array $data
     * @return array
     */
    protected function handleDataRule($data)
    {
        if (isset($data['rule'])) {
            $data['conditions'] = $data['rule']['conditions'];
            unset($data['rule']);
        }
        return $data;
    }

    /**
     * Handle when enable single store mode.
     *
     * @param array $data
     * @return void
     */
    protected function handleSingleStoreMode(&$data)
    {
        if ($data['id_store'] == null || !isset($data['id_store'])) {
            $data['id_store'] = 0;
        }
    }

    /**
     * Save
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $stringDisplay = isset(
            $data["display_popup"]
        ) ? $this->treatString($data["display_popup"]) : $this->getDefaultDisplay();
        $data["display_popup"] = $stringDisplay;
        $overwrite = isset($data["overwrite"]) ? "1" : "0";
        $this->handleSingleStoreMode($data);
        if ($data) {
            try {
                $modelRule = $this->ruleFactory->create();
                $id = $this->getRequest()->getParam('size_chart_value_id');
                $data = $this->handleDataRule($data);
                if ($id) {
                    $model = $this->sizeChartRepository->getById($id);
                    $idRule = $this->resourceRule->getRuleBySizeChartId($id);
                    if ($idRule) {
                        $modelRule->load($idRule);
                    }
                } else {
                    $model = $this->sizeChartRepository->getById();
                }
                $modelRule->loadPost($data);
                $modelRule->save();
                $model->setData($data);
                $this->sizeChartRepository->save($model);
                $sizeChartId = $model->getId();
                $ruleId = $modelRule->getId();
                $this->resourceRule->insertDataRule($ruleId, $sizeChartId);
                $data['products'] = $modelRule->getProductIds();
                $dataProductSizeChart = [];
                if ($overwrite) {
                    $this->resourceSizeChart->updateOverWrireSizeChart('0', $data['products']);
                }

                foreach ($data['products'] as $productId) {
                    $dataProductSizeChart[] = [
                        'size_chart_id' => $sizeChartId,
                        'product_id' => $productId,
                        'overwrite' => $overwrite
                    ];
                }
                $whereProductSizeChart = [
                    'size_chart_id = ?' => $sizeChartId
                ];
                $this->resourceSizeChart->deleteMultiData('bss_product_sizechart', $whereProductSizeChart);
                $this->resourceSizeChart->insertMultiData('bss_product_sizechart', $dataProductSizeChart);
                $this->messageManager->addSuccessMessage(__('The size chart has been saved.'));
                $this->helper->setSessionForm();
                return $this->redirectBackEditPage($model->getId(), $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __($e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'size_chart_value_id' => $this->getRequest()->getParam('size_chart_value_id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Redirect Back Edit Page
     *
     * @param int $id
     * @param mixed $resultRedirect
     * @return mixed
     */
    protected function redirectBackEditPage($id, $resultRedirect)
    {
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', [
                'size_chart_value_id' => $id,
                '_current' => true
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
