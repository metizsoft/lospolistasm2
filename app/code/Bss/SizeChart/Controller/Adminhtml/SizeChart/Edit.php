<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Controller\Adminhtml\SizeChart;

use Magento\Backend\App\Action;
use Magento\Backend\Model\SessionFactory;
use Bss\SizeChart\Model\SizeChartFactory;
use Bss\SizeChart\Model\ResourceModel;

/**
 * Class Edit
 *
 * @package Bss\SizeChart\Controller\Adminhtml\SizeChart
 */
class Edit extends Action
{

    /**
     * const ADMIN_RESOURCE
     */
    const ADMIN_RESOURCE = 'Magento_Backend::admin';

    /**
     * Registry
     *
     * @var \Magento\Framework\Registry|null
     */
    protected $coreRegistry = null;

    /**
     * SessionFactory
     *
     * @var SessionFactory
     */
    protected $sessionFactory;

    /**
     * PageFactory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Size Chart Factory
     *
     * @var SizeChartFactory
     */
    protected $sizeChartFactory;

    /**
     * ResourceModel\Rule
     *
     * @var ResourceModel\Rule $resourceRule
     */
    protected $resourceRule;

    /**
     * RuleFactory
     *
     * @var \Bss\SizeChart\Model\RuleFactory
     */
    protected $ruleFactory;

    /**
     * Edit constructor.
     *
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @param SessionFactory $sessionFactory
     * @param SizeChartFactory $sizeChartFactory
     * @param \Bss\SizeChart\Model\RuleFactory $ruleFactory
     * @param ResourceModel\Rule $resourceRule
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        SessionFactory $sessionFactory,
        SizeChartFactory $sizeChartFactory,
        \Bss\SizeChart\Model\RuleFactory $ruleFactory,
        ResourceModel\Rule $resourceRule
    ) {
        $this->sessionFactory = $sessionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->sizeChartFactory = $sizeChartFactory;
        $this->resourceRule = $resourceRule;
        $this->ruleFactory = $ruleFactory;
        parent::__construct($context);
    }


    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Bss_SizeChart::sizechart')
            ->addBreadcrumb(__('Size Chart'), __('Size Chart'))
            ->addBreadcrumb(
                __('Size Chart Infomation'),
                __('Size Chart Infomation')
            );
        return $resultPage;
    }

    /**
     * Edit Size Chart
     *
     * @return  \Magento\Backend\Model\View\Result\Page|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('size_chart_value_id');

        $model = $this->sizeChartFactory->create();
        $modelRule = $this->ruleFactory->create();
        if ($id) {
            $model->load($id);

            $ruleId = $this->resourceRule->getRuleBySizeChartId($id);

            if ($ruleId) {
                $modelRule = $modelRule->load($ruleId);
            }

            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(
                    __('This Size Chart no longer exists.')
                );
                /** \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->sessionFactory->create()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->coreRegistry->register('sizechart', $model);
        $this->coreRegistry->register('rule', $modelRule);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Size Chart') : __('New Size Chart'),
            $id ? __('Edit Size Chart') : __('New Size Chart')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Size Chart'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Size Chart'));

        return $resultPage;
    }
}
