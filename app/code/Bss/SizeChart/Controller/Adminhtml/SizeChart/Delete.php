<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Controller\Adminhtml\SizeChart;

use Magento\Backend\App\Action;
use Bss\SizeChart\Model\SizeChartFactory;
use Bss\SizeChart\Model\ResourceModel;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;

/**
 * Class Delete
 *
 * @package Bss\SizeChart\Controller\Adminhtml\SizeChart
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * const admin resource.
     */
    const ADMIN_RESOURCE = 'Bss_SizeChart::delete';

    /**
     * Attribute
     *
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * Resource Size Chart
     *
     * @var ResourceModel\SizeChart
     */
    protected $resourceSizeChart;

    /**
     * SizeChartFactory
     *
     * @var SizeChartFactory
     */
    protected $sizeChartFactory;

    /**
     * Delete constructor.
     *
     * @param Action\Context $context
     * @param SizeChartFactory $sizeChartFactory
     * @param ResourceModel\SizeChart $resourceSizeChart
     * @param Attribute $eavAttribute
     * @return void
     */
    public function __construct(
        Action\Context $context,
        SizeChartFactory $sizeChartFactory,
        ResourceModel\SizeChart $resourceSizeChart,
        Attribute $eavAttribute
    ) {
        parent::__construct($context);
        $this->eavAttribute = $eavAttribute;
        $this->resourceSizeChart = $resourceSizeChart;
        $this->sizeChartFactory = $sizeChartFactory;
    }

    /**
     * Delete product Assign Size Chart
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteProductAssignSizeChart($idSizeChart)
    {
        $table = $this->resourceSizeChart->getTable('catalog_product_entity_varchar');
        $attributeId = $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
        $where = [
            "value = ?" => (int)$idSizeChart,
            "attribute_id = ?" => (int)$attributeId
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete Product Assign Rule
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteProductAssignRule($idSizeChart)
    {
        $table = $this->resourceSizeChart->getTable('bss_product_sizechart');
        $where = [
            "size_chart_id = ?" => (int)$idSizeChart,
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete rule size chart
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteRuleSizeChart($idSizeChart)
    {
        $table = $this->resourceSizeChart->getTable('bss_sc_rule');
        $where = [
            "size_chart_id = ?" => (int)$idSizeChart,
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete Size Chart
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('size_chart_value_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // init model and delete
                $model = $this->sizeChartFactory->create();
                $model->load($id);
                $model->delete();
                $this->deleteProductAssignSizeChart($id);
                $this->deleteRuleSizeChart($id);
                $this->deleteProductAssignSizeChart($id);
                // display success message
                $this->messageManager->addSuccessMessage(
                    __('The size chart has been deleted.')
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['size_chart_value_id' => $id]
                );
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a size chart to delete.')
        );
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
