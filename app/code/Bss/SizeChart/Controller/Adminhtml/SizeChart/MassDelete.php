<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Controller\Adminhtml\SizeChart;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Bss\SizeChart\Model\ResourceModel\SizeChart\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Bss\SizeChart\Model\SizeChart;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Bss\SizeChart\Model\ResourceModel;

/**
 * Class MassDelete
 *
 * @package Bss\SizeChart\Controller\Adminhtml\SizeChart
 */
class MassDelete extends \Magento\Backend\App\Action
{

    /**
     * ADMIN_RESOURCE
     */
    const ADMIN_RESOURCE = 'Bss_SizeChart::delete';

    /**
     * Resource Size Chart
     *
     * @var ResourceModel\SizeChart
     */
    protected $resourceSizeChart;

    /**
     * Attribute
     *
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * Filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * CollectionFactory
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Size Chart
     *
     * @var SizeChart
     */
    protected $model;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param SizeChart $model
     * @param CollectionFactory $collectionFactory
     * @param Attribute $eavAttribute
     * @param ResourceModel\SizeChart $resourceSizeChart
     */
    public function __construct(
        Context $context,
        Filter $filter,
        SizeChart $model,
        CollectionFactory $collectionFactory,
        Attribute $eavAttribute,
        ResourceModel\SizeChart $resourceSizeChart
    ) {

        $this->eavAttribute = $eavAttribute;
        $this->model = $model;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resourceSizeChart = $resourceSizeChart;
        parent::__construct($context);
    }

    /**
     * Mass Delete Size Charts
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $auctionProduct) {
                $this->deleteSizeChart($auctionProduct);
            }
            $this->messageManager->addSuccessMessage(
                __(
                    'A total of %1 record(s) have been deleted.',
                    $collectionSize
                )
            );
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * Delete product Assign Size Chart
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteProductAssignSizeChart($idSizeChart)
    {

        $table = $this->resourceSizeChart->getTable('catalog_product_entity_varchar');
        $attributeId = $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
        $where = [
            "value = ?" => (int)$idSizeChart,
            "attribute_id = ?" => (int)$attributeId
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete Product Assign Rule
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteProductAssignRule($idSizeChart)
    {
        $table = $this->resourceSizeChart->getTable('bss_product_sizechart');
        $where = [
            "size_chart_id = ?" => (int)$idSizeChart,
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete Rule Size Chart
     *
     * @param int $idSizeChart
     * @return void
     */
    protected function deleteRuleSizeChart($idSizeChart)
    {
        $table = $this->resourceSizeChart->getTable('bss_sc_rule');
        $where = [
            "size_chart_id = ?" => (int)$idSizeChart,
        ];
        $this->resourceSizeChart->getConnection()->delete($table, $where);
    }

    /**
     * Delete size chart
     *
     * @param \Magento\Catalog\Model\Product $product
     * @throws \Exception
     */
    protected function deleteSizeChart($product)
    {
        $this->model->load($product->getId());
        $this->model->delete();
        $this->deleteProductAssignSizeChart($product->getId());
        $this->deleteProductAssignRule($product->getId());
        $this->deleteRuleSizeChart($product->getId());
    }
}
