<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Controller\Adminhtml\SizeChart;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Bss\SizeChart\Model\ResourceModel\SizeChart\CollectionFactory;
use Bss\SizeChart\Model\SizeChart;

/**
 * Class MassEnable
 *
 * @package Bss\SizeChart\Controller\Adminhtml\SizeChart
 */
class MassEnable extends Action
{

    /**
     * ADMIN_RESOURCE
     */
    const ADMIN_RESOURCE = 'Bss_SizeChart::save';

    /**
     * Filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * CollectionFactory
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Model
     *
     * @var SizeChart
     */
    protected $model;

    /**
     * MassEnable constructor.
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param SizeChart $model
     * @return void
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        SizeChart $model
    ) {
        $this->model = $model;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection=$this->filter->getCollection($this->collectionFactory->create());

        foreach ($collection as $auctionProduct) {
            $this->setEnableSizeChart($auctionProduct);
        }

        $this->messageManager->addSuccessMessage(
            __(
                'A total of %1 record(s) have been enabled.',
                $collection->getSize()
            )
        );

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Set Enable Size Chart
     *
     * @param \Magento\Catalog\Model\Product $product
     * @throws \Exception
     */
    protected function setEnableSizeChart($product)
    {
        $this->model->load($product->getId());
        $this->model->setEnable("1");
        $this->model->save();
    }
}
