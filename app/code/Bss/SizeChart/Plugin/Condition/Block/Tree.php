<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    BSS_GuestToCustomer
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Plugin\Condition\Block;

/**
 * Class Tree
 *
 * @package Bss\SizeChart\Plugin\Condition\Block
 */
class Tree
{

    /**
     * const
     */
    const JS_FORM_OBJECT = 'rule_form';
    const VERSION_224 = '2.2.4';

    /**
     * Product Metadata Interface
     *
     * @var \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    protected $productMetadata;

    /**
     * Tree constructor.
     *
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->productMetadata = $productMetadata;
    }

    /**
     * After get template
     *
     * @param \Magento\Catalog\Block\Adminhtml\Category\Tree $subject
     * @param mixed $result
     * @return string
     */
    public function afterGetTemplate(
        \Magento\Catalog\Block\Adminhtml\Category\Tree $subject,
        $result
    ) {
        $jsFormObject = $subject->getData('js_form_object');
        if ($this->productMetadata->getVersion() == self::VERSION_224 &&
            strpos($jsFormObject, self::JS_FORM_OBJECT) !== false
        ) {
            return 'Bss_SizeChart::catalog/category/tree.phtml';
        } else {
            return $result;
        }
    }
}
