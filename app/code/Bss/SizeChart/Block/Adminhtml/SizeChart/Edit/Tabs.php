<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block\Adminhtml\SizeChart\Edit;

/**
 * Class Tabs
 *
 * @package Bss\SizeChart\Block\Adminhtml\SizeChart\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sizechart_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('EDIT SIZE CHART'));
    }

    /**
     * Before html
     *
     * @return \Magento\Backend\Block\Widget\Tabs
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'conditions',
            [
                'label' => __('Assign Product'),
                'title' => __('Assign Product'),
                'content' => $this->getLayout()->createBlock(
                    \Bss\SizeChart\Block\Adminhtml\SizeChart\Edit\Tab\Conditions::class
                )->toHtml(),
            ]
        );
        return parent::_beforeToHtml();
    }
}
