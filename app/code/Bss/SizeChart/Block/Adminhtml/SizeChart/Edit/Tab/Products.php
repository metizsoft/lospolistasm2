<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block\Adminhtml\SizeChart\Edit\Tab;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Bss\SizeChart\Model\ResourceModel;
use Bss\SizeChart\Model\SizeChartFactory;

/**
 * Class Products
 *
 * @package Bss\SizeChart\Block\Adminhtml\SizeChart\Edit\Tab
 */
class Products extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * Eav Attribute
     *
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * Resource Size Chart
     *
     * @var ResourceModel\SizeChart
     */
    protected $resourceModel;

    /**
     * CollectionFactory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * ProductFactory
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * SizeChartFactory
     *
     * @var SizeChartFactory
     */
    protected $sizeChartFactory;

    /**
     * Registry
     *
     * @var  \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * Products constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $registry
     * @param SizeChartFactory $sizeChartFactory
     * @param ProductFactory $productFactory
     * @param CollectionFactory $productCollectionFactory
     * @param ResourceModel\SizeChart $resourceModel
     * @param Attribute $eavAttribute
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $registry,
        SizeChartFactory $sizeChartFactory,
        ProductFactory $productFactory,
        CollectionFactory $productCollectionFactory,
        ResourceModel\SizeChart $resourceModel,
        Attribute $eavAttribute,
        array $data = []
    ) {
        $this->resourceModel = $resourceModel;
        $this->productFactory = $productFactory;
        $this->sizeChartFactory = $sizeChartFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->registry = $registry;
        $this->eavAttribute = $eavAttribute;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('size_chart_value_id')) {
            $this->setDefaultFilter(['in_product' => 1]);
        }
    }


    /**
     * Add Column Filter To Collection
     *
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this|\Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_product') {
            $productIds = $this->getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter(
                    'entity_id',
                    ['in' => $productIds]
                );
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter(
                        'entity_id',
                        ['nin' => $productIds]
                    );
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * Prepare Collection
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('price');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare Column
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_product',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_product',
                'align' => 'center',
                'index' => 'entity_id',
               'values' => $this->_getSelectedProducts(),
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                'header' => __('Product ID'),
                'type' => 'number',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'index' => 'price',
                'width' => '50px',
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * Get Attribute Id
     *
     * @return int
     */
    protected function getAttributeId()
    {
        return $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
    }

    /**
     * Get Selected Products
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getSelectedProducts()
    {
        $arrayProductSizeChart = [];
        $arrayProductChance = [];
        $sizeChart = $this->getSizeChart();

        if (isset($sizeChart)) {
            $arrayProductSizeChart = $this->resourceModel->getArrayProductBySizeChartStore(
                $sizeChart->getIdStore(),
                $sizeChart->getId(),
                $this->getAttributeId()
            );
        }

        foreach ($arrayProductSizeChart as $productId) {
            array_push($arrayProductChance, $productId["entity_id"]);
        }

        return $arrayProductChance;
    }

    /**
     * Get Selected Products
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    protected function _getSelectedProducts()
    {
        $arrayProductSizeChart = [];
        $arrayProductChance = [];
        $sizeChart = $this->getSizeChart();
        if (isset($sizeChart)) {
            $arrayProductSizeChart = $this->resourceModel->getArrayProductBySizeChartStore(
                $sizeChart->getIdStore(),
                $sizeChart->getId(),
                $this->getAttributeId()
            );
        }
        foreach ($arrayProductSizeChart as $productId) {
            array_push($arrayProductChance, $productId["entity_id"]);
        }

        return $arrayProductChance;
    }

    /**
     * Get Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/productsgrid', ['_current' => true]);
    }

    /**
     * GetSizeChart
     *
     * @return \Bss\SizeChart\Model\SizeChart
     */
    protected function getSizeChart()
    {
        $sizeChartId = $this->getRequest()->getParam('size_chart_value_id');
        $sizeChart   = $this->sizeChartFactory->create();
        if ($sizeChartId) {
            $sizeChart->load($sizeChartId);
        }
        return $sizeChart;
    }

    /**
     * Can Show Tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return true;
    }
}
