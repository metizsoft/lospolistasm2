<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block\Adminhtml\SizeChart\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;

/**
 * Class Main
 *
 * @package Bss\SizeChart\Block\Adminhtml\SizeChart\Edit\Tab
 */
class Main extends Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * Store
     * @var \Magento\Store\Model\System\Store
     */
    protected $systemStore;

    /**
     * Yesno
     * @var Magento\Config\Model\Config\Source\Yesno
     */
    protected $status;

    /**
     * Config
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $wysiwygConfig;

    /**
     * Resource Size Chart
     * @var \Bss\SizeChart\Model\ResourceModel\SizeChart
     */
    protected $resourceSizeChart;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Config\Model\Config\Source\Yesno $yesno
     * @param \Bss\SizeChart\Model\ResourceModel\SizeChart $resourceSizeChart
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        \Bss\SizeChart\Model\ResourceModel\SizeChart $resourceSizeChart,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        $this->status = $yesno;
        $this->wysiwygConfig = $wysiwygConfig;
        $this->resourceSizeChart = $resourceSizeChart;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * PrepareForm
     * @return Generic
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _prepareForm()
    {
        /* @var $model \Bss\SizeChart\Model\SizeChart */
        $model = $this->_coreRegistry->registry('sizechart');

        $isElementDisabled = false;

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Edit Size Chart')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'size_chart_value_id',
                'hidden',
                ['name' => 'size_chart_value_id']
            );
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'enable',
            'select',
            [
                'label' => __('Enabled'),
                'title' => __('Enabled'),
                'name' => 'enable',
                'required' => true,
                'options' => $this->status->toArray(),
                'disabled' => $isElementDisabled
            ]
        );
        if (!$model->getId()) {
            $model->setData('enable', $isElementDisabled ? '0' : '1');
        }

        $config['document_base_url'] = $this->getData('store_media_url');
        $config['store_id'] = $this->getData('store_id');
        $config['add_variables'] = false;
        $config['add_widgets'] = false;
        $config['add_directives'] = true;
        $config['use_container'] = true;
        $config['container_class'] = 'hor-scroll';
    
        $fieldset->addField(
            'content',
            'editor',
            [
                'name'  => 'content',
                'label' => __('Edit Content'),
                'title' => __('Edit Content'),
                'style' => 'height:24em;',
                'wysiwyg'   => true,
                'required' => true,
                'force_load' => false,
                'config' => $this->wysiwygConfig->getConfig($config)
            ]
        );

        $fieldset->addField(
            'display_popup',
            'multiselect',
            [
                'name' => 'display_popup',
                'label' => __('Display'),
                'title' => __('Display'),
                'required' => false,
                'values' => $this->getDisplayPopup()
            ]
        );

        $fieldset->addField(
            'priority',
            'text',
            [
                'name' => 'priority',
                'label' => __('Priority'),
                'title' => __('Priority'),
                'required' => true,
                'class' => 'validate-number'
            ]
        );

        $fieldset->addField(
            'overwrite',
            'checkbox',
            [
                'name' => 'overwrite',
                'label' => __('Overwrite'),
                'title' => __('Overwrite'),
                'required' => false,
                'onclick' => 'this.value = this.checked ? 1 : 0;',
                'checked' => $this->checkSizeChartOverWrite($model->getId())
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                'id_store',
                'select',
                [
                    'name' => 'id_store',
                    'label' => __('Store View'),
                    'title' => __('Store View'),
                    'required' => true,
                    'values' =>$this->systemStore->getStoreValuesForForm(false, true)
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                \Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element::class
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'id_store',
                'hidden',
                [
                    'name'=>'id_store',
                    'value'=>$this->_storeManager->getStore(true)->getId()
                ]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }

        /**
         * Add Field Override Product Setting
         */
        $fieldset->addField(
            'override_product_setting',
            'select',
            [
                'label' => __('Override Product Settings'),
                'title' => __('Override Product Settings'),
                'name' => 'override_product_setting',
                'options' => $this->status->toArray()
            ]
        );


        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Get Display Popup
     * @return array
     */
    public function getDisplayPopup()
    {
        $arrDisplay = [
            ["label" => __('In popup'),"value" => "0"],
            ["label" => __('On Information Section'),"value" => "1"],
            ["label" => __('Under Add To Cart Button'),"value" => "2"]
        ];
        return $arrDisplay;
    }

    protected function checkSizeChartOverWrite($sizeChartId)
    {
        if ($sizeChartId) {
            $arrSizeChartIds = $this->resourceSizeChart->getSizeChartsOverWrite();
            return in_array($sizeChartId, $arrSizeChartIds);
        }
        return false;
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Edit Size Chart');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Edit Size Chart');
    }

    /**
     * Can Show Tab
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
