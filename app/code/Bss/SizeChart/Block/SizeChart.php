<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Bss\SizeChart\Model as ModelSizeChart;
use Magento\Cms\Model\Template\FilterProvider;
use Bss\SizeChart\Helper\Data as HelperData;

/**
 * Abstract class SizeChart
 *
 * @package Bss\SizeChart\Block
 */
abstract class SizeChart extends Template
{

    /**
     * HelperData
     *
     * @var HelperData $helper
     */
    protected $helper;

    /**
     * Registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * SizeChart Repository
     *
     * @var ModelSizeChart\SizeChartRepository $sizeChartRepository
     */
    protected $sizeChartRepository;

    /**
     * Filter Provider
     *
     * @var FilterProvider $filterProvider
     */
    protected $filterProvider;

    /**
     * SizeChart constructor.
     *
     * @param HelperData $helper
     * @param Registry $registry
     * @param ModelSizeChart\SizeChartRepository $sizeChartRepository
     * @param Template\Context $context
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        HelperData $helper,
        Registry $registry,
        ModelSizeChart\SizeChartRepository $sizeChartRepository,
        Template\Context $context,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->coreRegistry = $registry;
        $this->sizeChartRepository = $sizeChartRepository;
        $this->filterProvider = $filterProvider;
    }

    /**
     * EditContentSizeChart
     *
     * @param string $stringContent
     * @return string
     */
    public function editContentSizeChart($stringContent)
    {
        try {
            return $this->filterProvider->getPageFilter()->filter($stringContent);
        } catch (\Exception $exception) {
            $this->helper->writeLog($exception->getMessage());
            return '';
        }
    }

    /**
     * Get Size Chart Product Id By Store
     *
     * @param int $productId
     * @param int $storeId
     * @return array
     */
    public function getSizeChartProductIdByStore($productId, $storeId)
    {
        try {
            return $this->sizeChartRepository->getSizeChartProductIdByStore($productId, $storeId);
        } catch (\Exception $exception) {
            $this->helper->writeLog($exception->getMessage());
            return [];
        }
    }

    /**
     * Get All Size Chart By Store View
     *
     * @param int $productId
     * @param int $storeId
     * @return ModelSizeChart\SizeChart
     */
    public function getAllSizeChartByStoreView($productId, $storeId)
    {
        return $this->sizeChartRepository->getAllSizeChartByStoreView($productId, $storeId);
    }

    /**
     * Get current store.
     *
     * @return int|null
     */
    public function getStoreId()
    {
        try {
            return $this->_storeManager->getStore()->getId();
        } catch (\Exception $exception) {
            $this->helper->writeLog($exception->getMessage());
            return null;
        }
    }

    /**
     * Check Store View
     *
     * @param int $idStoreSizeChart
     * @param int $idStoreFront
     * @return bool
     */
    public function checkStore($idStoreSizeChart, $idStoreFront)
    {
        $arrayIdStoreSizeChart = explode(',', $idStoreSizeChart);
        return in_array($idStoreFront, $arrayIdStoreSizeChart);
    }

    /**
     * Check Display Size Chart
     *
     * @param int $idDisplaySizeChart
     * @param int $idDisplayPostion
     * @return bool
     */
    public function checkDisplay($idDisplaySizeChart, $idDisplayPostion)
    {
        $arrIdDisplay = explode(',', $idDisplaySizeChart);
        return in_array($idDisplayPostion, $arrIdDisplay);
    }

    /**
     * Get Product
     *
     * @return  \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        $product = $this->getData('product');

        return $product;
    }
}
