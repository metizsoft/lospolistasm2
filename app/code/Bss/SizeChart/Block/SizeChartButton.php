<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block;

/**
 * Class SizeChartButton
 *
 * @package Bss\SizeChart\Block
 */
class SizeChartButton extends SizeChart
{
    /**
     * Get Color
     *
     * @return string
     */
    public function getColorTextPopup()
    {
        return  $this->helper->getConfigColor();
    }

    /**
     * Get Title Size Chart
     *
     * @return string
     */
    public function getTitle()
    {
        return  $this->helper->getConfigTitle();
    }

    /**
     * Get Icon
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getIcon()
    {
        $icon =  $this->helper->getConfigIcon();
        if (!isset($icon)) {
            return false;
        }
        $mediaUrl = $this->_storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl . 'bss/sizechart/' . $icon;
    }

}
