<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ProductCustomTabs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\SizeChart\Model;

/**
 * Class Rule
 *
 * @package Bss\SizeChart\Model
 */
class Rule extends \Magento\Rule\Model\AbstractModel
{
    /**
     * Store matched product Ids
     *
     * @var array
     */
    protected $productIds;

    /**
     * Store matched product Ids with rule id
     *
     * @var array
     */
    public $dataProductIds;

    /**
     * Resource iterator
     *
     * @var \Magento\Framework\Model\ResourceModel\Iterator
     */
    protected $resourceIterator;

    /**
     * Helper data
     *
     * @var \Bss\SizeChart\Helper\Data $helperData
     */
    protected $helperData;

    /**
     * Rule constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Bss\SizeChart\Helper\Data $helperData
     * @param \Magento\Framework\Model\ResourceModel\Iterator $resourceIterator
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Bss\SizeChart\Helper\Data $helperData,
        \Magento\Framework\Model\ResourceModel\Iterator $resourceIterator,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->resourceIterator = $resourceIterator;
        $this->helperData = $helperData;
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $localeDate,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Construct
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Bss\SizeChart\Model\ResourceModel\Rule::class);
        $this->setIdFieldName('rule_id');
    }

    /**
     * Get Conditions Instance
     *
     * @return \Magento\CatalogRule\Model\Rule\Condition\Combine
     */
    public function getConditionsInstance()
    {
        return $this->helperData->getConditionCombine();
    }

    /**
     * Get Actions Instance
     *
     * @return \Magento\CatalogRule\Model\Rule\Condition\Combine
     */
    public function getActionsInstance()
    {
        return $this->helperData->getConditionCombine();
    }

    /**
     * Get Conditions Field Set Id
     *
     * @param string $formName
     * @return string
     */
    public function getConditionsFieldSetId($formName = '')
    {
        return $formName . 'rule_conditions_fieldset_' . $this->getId();
    }

    /**
     * Get Actions Field Set Id
     *
     * @param string $formName
     * @return string
     */
    public function getActionsFieldSetId($formName = '')
    {
        return $formName . 'rule_actions_fieldset_' . $this->getId();
    }

    /**
     * After Save
     *
     * @return \Magento\Rule\Model\AbstractModel
     */
    public function afterSave()
    {
        $this->getMatchingProductIds();
        return parent::afterSave();
    }

    /**
     * Get Product Ids
     *
     * @return array
     */
    public function getProductIds()
    {
        return $this->productIds;
    }

    /**
     * Get array of product ids which are matched by rule
     *
     * @return array
     */
    public function getMatchingProductIds()
    {
        if ($this->productIds === null) {
            $this->productIds = [];
            $this->setCollectedAttributes([]);
            if ($this->helperData->getWebsiteIds()) {
                /** @var $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
                $productCollection = $this->helperData->getProduct()->getResourceCollection();

                $productCollection->addAttributeToFilter(
                    'status',
                    ['in' => $this->helperData->getVisibleStatusIds()]
                );
                $productCollection->addWebsiteFilter($this->helperData->getWebsiteIds());
                $this->getConditions()->collectValidatedAttributes($productCollection);

                $this->resourceIterator->walk(
                    $productCollection->getSelect(),
                    [[$this, 'callbackValidateProduct']],
                    [
                        'attributes' => $this->getCollectedAttributes(),
                        'product' => $this->helperData->getProduct()
                    ]
                );
            }
        }

        return $this->productIds;
    }

    /**
     * Callback function for product matching
     *
     * @param array $args
     * @return void
     */
    public function callbackValidateProduct($args)
    {
        $product = clone $args['product'];
        $product->setData($args['row']);
        $ruleId = $this->getRuleId();
        if ($ruleId && $this->getConditions()->validate($product)) {
            $this->productIds[] = $product->getId();
        }
    }
}
