<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model;

use Bss\SizeChart\Api as SizeChartApi;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;

/**
 * Class SizeChartRepository
 *
 * @package Bss\SizeChart\Model
 */
class SizeChartRepository implements SizeChartApi\SizeChartRepositoryInterface
{

    /**
     * SizeChart Factory
     *
     * @var \Bss\SizeChart\Model\SizeChartFactory $sizeChartFactory
     */
    protected $sizeChartFactory;

    /**
     * Attribute
     *
     * @var Attribute $eavAttribute
     */
    protected $eavAttribute;

    /**
     * Model
     *
     * @var SizeChart $model
     */
    protected $model;

    /**
     * ResourceModel
     *
     * @var ResourceModel\SizeChart $resourceModel
     */
    protected $resourceModel;

    /**
     * SizeChartRepository constructor.
     *
     * @param SizeChartFactory $sizeChartFactory
     * @param Attribute $eavAttribute
     * @param SizeChart $model
     * @param ResourceModel\SizeChart $resourceModel
     * @return void
     */
    public function __construct(
        \Bss\SizeChart\Model\SizeChartFactory $sizeChartFactory,
        Attribute $eavAttribute,
        SizeChart $model,
        ResourceModel\SizeChart $resourceModel
    ) {
        $this->sizeChartFactory = $sizeChartFactory;
        $this->eavAttribute = $eavAttribute;
        $this->model = $model;
        $this->resourceModel = $resourceModel;
    }

    /**
     * Get Size Chart Product Id By Store
     *
     * @param int $idProduct
     * @param int $idStore
     * @return array
     */
    public function getSizeChartProductIdByStore($idProduct, $idStore)
    {
        $table = $this->resourceModel->getTable('catalog_product_entity_varchar');
        $attributeId = $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
        $sql = $this->resourceModel->buildSql($table, $idProduct, $attributeId, $idStore);
        $result = $this->resourceModel->getConnection()->fetchRow($sql);
        return $result['value'];
    }

    /**
     * Get all size chart by store view
     *
     * @param int $productId
     * @param int $storeId
     * @return \Bss\SizeChart\Model\SizeChart
     */
    public function getAllSizeChartByStoreView($productId, $storeId)
    {
        $sizeChartId = $this->resourceModel->getSizeChartsByStoreView($productId, $storeId);
        $sizeChartAttributeStore = $this->getSizeChartProductIdByStore($productId, $storeId);
        $sizeChartAttributeAllStore = $this->getSizeChartProductIdByStore($productId, 0);
        $sizeChartAttributeId = $sizeChartAttributeStore;
        if ($sizeChartAttributeId == false) {
            $sizeChartAttributeId = $sizeChartAttributeAllStore;
        }
        $sizeChartAttribute = $this->sizeChartFactory->create();
        $this->resourceModel->load($sizeChartAttribute, $sizeChartAttributeId);
        $sizeChart = $this->sizeChartFactory->create();
        $this->resourceModel->load($sizeChart, $sizeChartId);
        if ($sizeChart->getData('override_product_setting') == 1) {
            $sizeChartAttribute = $sizeChart;
        }
        return $sizeChartAttribute;
    }

    /**
     * Get size chart by id.
     *
     * @param int $id
     * @return SizeChart
     */
    public function getById($id = null)
    {
        $model = $this->sizeChartFactory->create();
        if ($id != null) {
            $this->resourceModel->load($model, $id);
        }
        return $model;
    }

    /**
     * Process save
     *
     * @param SizeChartApi\Data\SizeChartInterface $model
     * @return void
     */
    public function save(SizeChartApi\Data\SizeChartInterface $model)
    {
        $this->resourceModel->save($model);
    }
}
