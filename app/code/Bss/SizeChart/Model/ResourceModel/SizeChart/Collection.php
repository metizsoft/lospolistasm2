<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model\ResourceModel\SizeChart;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @package Bss\SizeChart\Model\ResourceModel\SizeChart
 */
class Collection extends AbstractCollection
{
    /**
     * Id Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'size_chart_value_id';

    /**
     * Construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Bss\SizeChart\Model\SizeChart::class,
            \Bss\SizeChart\Model\ResourceModel\SizeChart::class
        );
        $this->_map['fields']['size_chart_value_id'] = 'main_table.size_chart_value_id';
    }
}
