<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Psr\Log\LoggerInterface;

/**
 * Class SizeChart
 *
 * @package Bss\SizeChart\Model\ResourceModel
 */
class SizeChart extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * LoggerInterface
     *
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * ResourceConnection
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * AdapterInterface
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * SizeChart constructor.
     *
     * @param Context $context
     * @param LoggerInterface $logger
     * @param string $connectionName
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        $connectionName = null
    ) {
        $this->resource = $context->getResources();
        $this->connection = $this->resource->getConnection();
        $this->logger = $logger;
        parent::__construct($context, $connectionName);
    }

    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bss_size_chart', 'size_chart_value_id');
    }

    /**
     * Get Array Product By Size Chart Store
     *
     * @param int $idStore
     * @param int $idSizeChart
     * @param int $idAttribute
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getArrayProductBySizeChartStore($idStore, $idSizeChart, $idAttribute)
    {
        $table = $this->getTable('catalog_product_entity_varchar');

        $sql = $this->connection->select()->from(
            [$table],
            ["entity_id"]
        )->where("value = ?", $idSizeChart)->where("store_id = ?", $idStore)->where("attribute_id = ?", $idAttribute);
        $arrEntity = [];
        $query = $this->connection->query($sql);
        while ($row = $query->fetch()) {
            array_push($arrEntity, $row);
        }
        return $arrEntity;
    }

    /**
     * Get array sku by size chart store.
     *
     * @param int $idStore
     * @param int $idSizeChart
     * @param int $idAttribute
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getArraySkuBySizeChartStore($idStore, $idSizeChart, $idAttribute)
    {
        $tableEntityVarchar = $this->getTable('catalog_product_entity_varchar');
        $tableEntity = $this->getTable('catalog_product_entity');
        $sql = $this->connection->select()->from(
            ["main_table" => $tableEntityVarchar],
            ["entity_id"]
        )
            ->join(
                ['bptr' => $tableEntity],
                'main_table.entity_id = bptr.entity_id',
                ['*']
            )->where(
                "main_table.value = ?",
                $idSizeChart
            )->where(
                "main_table.store_id = ?",
                $idStore
            )->where(
                "main_table.attribute_id = ?",
                $idAttribute
            );
        $arrEntity = [];
        $query = $this->connection->query($sql);
        while ($row = $query->fetch()) {
            array_push($arrEntity, $row['sku']);
        }
        return $arrEntity;
    }

    /**
     * Get Arr Product By Size Chart
     *
     * @param int $idSizeChart
     * @param int $idAttribute
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getArrProductBySizeChart($idSizeChart, $idAttribute)
    {

        $tableEntityVarchar = $this->getTable('catalog_product_entity_varchar');

        $sql = $this->connection->select()->from(
            [$tableEntityVarchar],
            ["entity_id"]
        )->where("value = ?", $idSizeChart)->where("attribute_id = ?", $idAttribute);

        $arrEntity = [];
        $query = $this->connection->query($sql);
        while ($row = $query->fetch()) {
            array_push($arrEntity, $row);
        }
        return $arrEntity;
    }

    /**
     * Build Sql
     *
     * @param string $table
     * @param int $productId
     * @param int $attributeId
     * @param int $idStore
     * @return \Magento\Framework\DB\Select
     */
    public function buildSql($table, $productId, $attributeId, $idStore)
    {
        $tableName = $this->getTable($table);
        return $this->connection->select()->from(
            [$tableName],
            ["value"]
        )->where(
            "entity_id = ?",
            $productId
        )->where(
            "attribute_id = ?",
            $attributeId
        )->where(
            "store_id = ?",
            $idStore
        );
    }

    /**
     * Insert table product
     *
     * @param string $table
     * @param array $bind
     * @return void
     */
    public function insertProduct($table, $bind = [])
    {
        $tableName = $this->getTable($table);
        if (is_array($bind)) {
            $this->connection->insert($tableName, $bind);
        }
    }

    /**
     * Upgrade Data Rule
     *
     * @param string $tableName
     * @param string $condition
     */
    public function upgradeDataRule($tableName, $condition)
    {
        try {
            $adapter = $this->getConnection();
            $table = $this->getTable($tableName);
            $adapter->insert(
                $table,
                [
                    'conditions_serialized' => $condition
                ]
            );
        } catch (\Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
    }

    /**
     * Get Last Id Rule
     *
     * @return int
     */
    public function getLastIdRule()
    {
        $result = 0;
        try {
            $adapter = $this->getConnection();
            $table = $this->getTable('bss_rule');
            $sql = $adapter->select()->from(
                $table,
                'rule_id'
            )->order('rule_id DESC')->limit(1);

            $row = $adapter->fetchRow($sql);
            $result = $row['rule_id'];
        } catch (\Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
        return $result;
    }

    /**
     * Upgrade Data
     *
     * @param string $tableName
     * @param int $ruleId
     * @param int $sizeChartId
     * @return void
     */
    public function upgradeData($tableName, $ruleId, $sizeChartId)
    {
        try {
            $adapter = $this->getConnection();
            $table = $this->getTable($tableName);
            $arrData = [
                'rule_id' => $ruleId,
                'size_chart_id' => $sizeChartId
            ];
            $adapter->insert(
                $table,
                $arrData
            );
        } catch (\Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
    }

    /**
     * Insert Multi Data
     *
     * @param string $table
     * @param array $bind
     * @return void
     */
    public function insertMultiData($table, $bind)
    {
        $adapter = $this->getConnection();
        $table = $this->getTable($table);
        if (is_array($bind)) {
            $adapter->insertMultiple($table, $bind);
        }
    }

    /**
     * Get Size Charts By Store View
     *
     * @param int $productId
     * @param int $storeId
     * @return mixed
     */
    public function getSizeChartsByStoreView($productId, $storeId)
    {
        try {
            $adapter = $this->getConnection();
            $table = $this->getTable('bss_product_sizechart');
            $tableSizeChart = $this->getTable('bss_size_chart');
            $arrStores = ['0' , $storeId];
            $sql = $adapter->select()->from(
                ['main_table' => $tableSizeChart],
                ["*"]
            )->join(
                ['product_sizechart' => $table],
                'main_table.size_chart_value_id = product_sizechart.size_chart_id'
            )->where(
                'enable',
                1
            )->where(
                'product_id = ?',
                $productId
            )->where(
                'overwrite = ?',
                '1'
            )->where(
                'id_store in ( ? )',
                $arrStores
            );

            $result = $adapter->fetchRow($sql);
            if ($result) {
                return $result['size_chart_value_id'];
            } else {
                $sql = $adapter->select()->from(
                    ['main_table' => $tableSizeChart],
                    ["*"]
                )->join(
                    ['product_sizechart' => $table],
                    'main_table.size_chart_value_id = product_sizechart.size_chart_id'
                )->where(
                    'product_id = ?',
                    $productId
                )->where(
                    'enable',
                    1
                )->where(
                    'id_store in ( ? )',
                    $arrStores
                )->order(
                    'priority DESC'
                )->limit(
                    1
                );

                $result = $adapter->fetchRow($sql);
            }
        } catch (\Exception $exception) {
            return null;
        }
        return $result['size_chart_value_id'];
    }

    /**
     * Delete Multi Data
     *
     * @param string $table
     * @param array $where
     * @return void
     */
    public function deleteMultiData($table, $where)
    {
        $adapter = $this->getConnection();
        $table = $this->getTable($table);
        if (is_array($where)) {
            $adapter->delete($table, $where);
        }
    }

    /**
     * Update Over Write SizeChart
     *
     * @param int $overWrite
     * @param array $productIds
     * @return void
     */
    public function updateOverWrireSizeChart($overWrite, $productIds = [])
    {
        $adapter =$this->getConnection();
        $table = $this->getTable('bss_product_sizechart');
        foreach ($productIds as $productId) {
            $adapter->update(
                $table,
                ['overwrite' => $overWrite],
                ['product_id = ?' => $productId]
            );
        }
    }

    /**
     * Get Size Charts OverWrite
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getSizeChartsOverWrite()
    {
        $adapter = $this->getConnection();
        $table = $this->getTable('bss_product_sizechart');
        $sql = $adapter->select()->from($table)->where('overwrite = ?', '1');
        $arrSizeChartIds = [];
        $query = $this->connection->query($sql);
        while ($row = $query->fetch()) {
            array_push($arrSizeChartIds, $row['size_chart_id']);
        }
        $arrSizeChartIds = array_unique($arrSizeChartIds);
        return $arrSizeChartIds;
    }
}
