<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model\SizeChart\Source;

use Bss\SizeChart\Model\SizeChartFactory;
use Bss\SizeChart\Model\ResourceModel;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;

/**
 * Class SizeChart
 *
 * @package Bss\SizeChart\Model\SizeChart\Source
 */
class SizeChart extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Attribute Id
     *
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * Resource Size Chart
     *
     * @var ResourceModel\SizeChart
     */
    protected $resourceSizeChart;

    /**
     * SizeChartFactory
     *
     * @var SizeChartFactory
     */
    protected $sizeChartFactory;

    /**
     * Request
     *
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * SizeChart constructor.
     *
     * @param SizeChartFactory $sizeChartFactory
     * @param \Magento\Framework\App\Request\Http $request
     * @param ResourceModel\SizeChart $resourceSizeChart
     * @param Attribute $eavAttribute
     * @return void
     */
    public function __construct(
        SizeChartFactory $sizeChartFactory,
        \Magento\Framework\App\Request\Http $request,
        ResourceModel\SizeChart $resourceSizeChart,
        Attribute $eavAttribute
    ) {
        $this->resourceSizeChart = $resourceSizeChart;
        $this->sizeChartFactory = $sizeChartFactory;
        $this->request = $request;
        $this->eavAttribute = $eavAttribute;
    }

    /**
     * Get store id
     *
     * @return int $storeId
     */
    protected function getStoreId()
    {
        if ($this->request->getParam("store") == null) {
            $storeId = "0";
        } else {
            $storeId = $this->request->getParam("store");
        }
        return $storeId;
    }

    /**
     * Get attribute Id
     *
     * @return int $attributeId
     */
    protected function getAttributeId()
    {
        $attributeId = $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
        return $attributeId;
    }


    /**
     * Get Id Size Chart Current
     *
     * @return array $idSizeChart
     */
    protected function getIdSizeChartCurrent()
    {
        if ($this->request->getParam("id") == null) {
            $productId = "0";
        } else {
            $productId = $this->request->getParam("id");
        }
        $table = $this->resourceSizeChart->getTable('catalog_product_entity_varchar');
        $attributeId = $this->getAttributeId();
        $storeId = $this->getStoreId();
        $sql = $this->resourceSizeChart->buildSql($table, $productId, $attributeId, $storeId);
        return $this->resourceSizeChart->getConnection()->fetchRow($sql);
    }

    /**
     * Get All option size chart
     *
     * @return array
     */
    public function getAllOptions()
    {
        $idSizeChartCurrent = $this->getIdSizeChartCurrent();
        $priority = $this->sizeChartFactory->create()->load($idSizeChartCurrent)->getPriority();
        if (!$this->_options) {
            $collectionSizeChart = $this->sizeChartFactory->create()->getCollection();
            $this->_options = [];
            array_push(
                $this->_options,
                [
                    'label' => __(" "),
                    'value' => null
                ]
            );
            foreach ($collectionSizeChart as $sizeChart) {
                if ($sizeChart->getData('enable') == 1) {
                    $stringStoreIdSizeChart = $sizeChart->getData("id_store");
                    $arrStoreIdSizeChart = explode(",", $stringStoreIdSizeChart);
                    $storeId = $this->getStoreId();
                    if (in_array($storeId, $arrStoreIdSizeChart) || in_array("0", $arrStoreIdSizeChart)) {
                        if ($sizeChart->getPriority() >= $priority) {
                            array_push(
                                $this->_options,
                                [
                                    'label' => __($sizeChart->getTitle()),
                                    'value' => $sizeChart->getId()
                                ]
                            );
                        }
                    }
                }
            }
        }
        return $this->_options;
    }
}
