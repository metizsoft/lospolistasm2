<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model\Config;

/**
 * Class DropDownConfig
 *
 * @package Bss\SizeChart\Model\Config
 */
class DropDownConfig implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Display popup
     */
    const IN_POPUP = 0;

    /**
     * Information section
     */
    const INFORMATION_SECTION = 1;

    /**
     * Under add to cart
     */
    const UNDER_ADD_TO_CART = 2;

    /**
     * Option dropdown
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::IN_POPUP,
                'label' => __('In Popup')
            ],
            [
                'value' => self::INFORMATION_SECTION,
                'label' => __('On Information Section')
            ],
            [
                'value' => self::UNDER_ADD_TO_CART,
                'label' => __('Under Add To Cart Button')
            ]
        ];
    }
}
