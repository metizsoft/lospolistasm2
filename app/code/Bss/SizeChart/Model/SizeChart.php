<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model;

use Bss\SizeChart\Api\Data\SizeChartInterface;

/**
 * Class SizeChart
 *
 * @package Bss\SizeChart\Model
 */
class SizeChart extends \Magento\Framework\Model\AbstractModel implements SizeChartInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Bss\SizeChart\Model\ResourceModel\SizeChart::class);
    }

    /**
     * Set Content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->setData('content', $content);
    }

    /**
     * Get Content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->getData('content');
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->setData('title', $title);
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData('title');
    }

    /**
     * Set Enable
     *
     * @param int $enable
     * @return void
     */
    public function setEnable($enable)
    {
        $this->setData('enable', $enable);
    }

    /**
     * Get Enable
     *
     * @return int
     */
    public function getEnable()
    {
        return $this->getData('enable');
    }

    /**
     * Get Id Store
     *
     * @return int
     */
    public function getIdStore()
    {
        return $this->getData('id_store');
    }

    /**
     * Get Popup
     *
     * @return int
     */
    public function getPopUp()
    {
        return $this->getData('display_popup');
    }

    /**
     * Get Priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->getData('priority');
    }
}
