<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Helper;

use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Backend\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @package Bss\SizeChart\Helper
 */
class Data extends AbstractHelper
{

    /**
     * Const
     */
    const BSS_MESSAGE_DEBUG = 'Module Bss_SizeChart has few bugs.';

    /**
     * UrlInterface
     *
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $backendUrl;

    /**
     * StoreManagerInterface
     *
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;

    /**
     * ScopeConfigInterface
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    protected $scopeConfig;

    /**
     * Attribute
     *
     * @var Attribute $eavAttribute
     */
    protected $eavAttribute;

    /**
     * SessionFactory
     *
     * @var SessionFactory
     */
    protected $sessionFactory;

    /**
     * Product Status
     *
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $productStatus;

    /**
     * Condition Combine
     *
     * @var \Magento\CatalogRule\Model\Rule\Condition\CombineFactory
     */
    protected $condCombineFactory;

    /**
     * ProductFactory
     *
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Attribute $eavAttribute
     * @param SessionFactory $sessionFactory
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\CatalogRule\Model\Rule\Condition\CombineFactory $condCombineFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Attribute $eavAttribute,
        SessionFactory $sessionFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\CatalogRule\Model\Rule\Condition\CombineFactory $condCombineFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $context->getScopeConfig();
        $this->eavAttribute = $eavAttribute;
        $this->sessionFactory = $sessionFactory;
        $this->productStatus = $productStatus;
        $this->condCombineFactory = $condCombineFactory;
        $this->productFactory = $productFactory;
        parent::__construct($context);
    }

    /**
     * Get products tab Url in admin
     *
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->backendUrl->getUrl(
            'bss_size_chart_admin/sizechart/products',
            ['_current' => true]
        );
    }

    /**
     * Get Config Title
     *
     * @return string
     */
    public function getConfigTitle()
    {
        return  $this->scopeConfig->getValue(
            'bss_sizechart/bss_sizechart_setting/text_button_bss_sizechart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Config Icon
     *
     * @return string
     */
    public function getConfigIcon()
    {
        return $this->scopeConfig->getValue(
            'bss_sizechart/bss_sizechart_setting/button_icon_bss_sizechart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Config Color
     *
     * @return string
     */
    public function getConfigColor()
    {
        return  $this->scopeConfig->getValue(
            'bss_sizechart/bss_sizechart_setting/button_text_color',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Default Display
     *
     * @return string
     */
    public function getDefaultDisplay()
    {
        return $this->scopeConfig->getValue(
            'bss_sizechart/bss_sizechart_setting/default_display_bss_sizechart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Attribute Size Chart Id
     *
     * @return int
     */
    public function getAttributeSizeChartId()
    {
        return $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );
    }

    /**
     * Set Session Form
     *
     * @return void
     */
    public function setSessionForm()
    {
        $this->sessionFactory->create()->setFormData(false);
    }

    /**
     * Get Website Ids
     *
     * @return array
     */
    public function getWebsiteIds()
    {
        $arrWebsiteIds = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $arrWebsiteIds[] = $website->getId();
        }
        return $arrWebsiteIds;
    }

    /**
     * Get Visible Status Ids
     *
     * @return array
     */
    public function getVisibleStatusIds()
    {
        return $this->productStatus->getVisibleStatusIds();
    }

    /**
     * Get Condition Combine
     *
     * @return mixed
     */
    public function getConditionCombine()
    {
        return $this->condCombineFactory->create();
    }

    /**
     * Get Product
     *
     * @return mixed
     */
    public function getProduct()
    {
        return $this->productFactory->create();
    }

    /**
     * Write log of module Bss_SizeChart
     *
     * @param string $mes
     */
    public function writeLog($mes)
    {
        $this->_logger->debug(self::BSS_MESSAGE_DEBUG . $mes);
    }
}
