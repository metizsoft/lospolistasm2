<?php

namespace Magebees\Testimonial\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

          $table = $installer->getConnection()->newTable(
              $installer->getTable('magebees_customer_testimonials')
          )->addColumn(
              'testimonial_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
              'id'
          )->addColumn(
              'name',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'name'
          )->addColumn(
              'email',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'email'
          )->addColumn(
              'image',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'image'
          )->addColumn(
              'website',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'website'
          )->addColumn(
              'company',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'company'
          )->addColumn(
              'address',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'address'
          )->addColumn(
              'testimonial',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'testimonial'
          )->addColumn(
              'status',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['unsigned' => true,'nullable' => false],
              'status'
          )->addColumn(
              'stores',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'stores'
          )->addColumn(
              'ext_video',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['unsigned' => true,'nullable' => false],
              'ext_video'
          )->addColumn(
              'video_url',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['unsigned' => true,'nullable' => false],
              'video_url'
          )->addColumn(
              'rating',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['unsigned' => true,'nullable' => false],
              'rating'
          )->addColumn(
              'enabled_home',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['unsigned' => true,'nullable' => false],
              'enabled'
          )->addColumn(
              'enabled_widget',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['unsigned' => true,'nullable' => false],
              'enabled'
          )->addColumn(
              'inserted_date',
              \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
              null,
              ['nullable' => false],
              'inserted_date'
          )->addColumn(
              'updated_date',
              \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
              null,
              ['nullable' => false],
              'updated_date'
          );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
