<?php

namespace Magebees\Testimonial\Model;

class Testimonialcollection extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magebees\Testimonial\Model\ResourceModel\Testimonialcollection');
    }
}
