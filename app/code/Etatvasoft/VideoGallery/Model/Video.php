<?php

/**
 * PHP version 7.1
 *
 * @category  Etatvasoft
 * @package   Etatvasoft\VideoGallery\Controller\Adminhtml\Video
 * @author    Etatvasoft <magento@etatvasoft.com>
 * @copyright 2018 This file was generated by Etatvasoft
 * @license   http://etatvasoft.com  Open Software License (OSL 3.0)
 * @link      http://etatvasoft.com
 */

namespace Etatvasoft\VideoGallery\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * PHP version 7.1
 *
 * @category  Etatvasoft
 * @package   Etatvasoft\VideoGallery\Controller\Adminhtml\Video
 * @author    Etatvasoft <magento@etatvasoft.com>
 * @copyright 2018 This file was generated by Etatvasoft
 * @license   http://etatvasoft.com  Open Software License (OSL 3.0)
 * @link      http://etatvasoft.com
 */
class Video extends AbstractModel
{
    /**
     * Initialize
     *
     * @return Null
     */
    protected function _construct()
    {
        $this->_init('Etatvasoft\VideoGallery\Model\ResourceModel\Video');
    }
}
