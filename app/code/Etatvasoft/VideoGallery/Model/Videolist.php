<?php
namespace Etatvasoft\VideoGallery\Model;

class Videolist implements \Magento\Framework\Option\ArrayInterface
{   

    public function __construct(\Etatvasoft\VideoGallery\Model\VideoFactory $videoFactory) {
     $this->_videoFactory = $videoFactory;
    }

    public function toOptionArray()
    {       
        $datacollection = $this->_videoFactory->create()->getCollection()
        ->addFieldToFilter('is_active', 1)
        ->setOrder('position', 'ASC')
        ->setOrder('updated_at', 'DESC');

        $gallary[] = ['value' =>'all', 'label' => __('All')];  
        foreach ($datacollection as $value) {
            $gallary[] = ['value' =>$value->getData('id'), 'label' => __($value->getData('title'))];
        }
        return $gallary;
    }
}
