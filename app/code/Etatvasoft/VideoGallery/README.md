##Video Gallery Extension

This extension is perfect marketing tool for your bussiness that provides Video Gallery to your existing customers.

##Support: 
version - 2.1.x, 2.2.x, 2.3

##How to install Extension

1. Download the archive file.
2. Unzip the file
3. Create a folder [root]/app/code/Etatvasoft/VideoGallery
4. Drop/move the unzipped folder to directory 'Magento_Root/app/code/Etatvasoft/VideoGallery'

#Enable Extension:
- php bin/magento module:enable Etatvasoft_VideoGallery
- php bin/magento setup:upgrade
- php bin/magento cache:clean
- php bin/magento setup:static-content:deploy
- php bin/magento cache:flush