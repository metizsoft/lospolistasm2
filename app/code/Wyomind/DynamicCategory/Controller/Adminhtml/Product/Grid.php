<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Controller\Adminhtml\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class Grid extends \Magento\Catalog\Controller\Adminhtml\Category\Grid
{

       protected $_indexer;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Wyomind\DynamicCategory\Helper\Indexer $indexer
    ) {
    

        $this->_indexer = $indexer;
               parent::__construct($context, $resultRawFactory, $layoutFactory);
    }

    public function execute()
    {
        $category = $this->_initCategory(true);
        if (!$category) {
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('catalog/*/', ['_current' => true, 'id' => null]);
        }
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        // Allow another (AJAX) requests to be made if this one is too long
        session_write_close();
        try {
            $productIds = $this->_indexer->process($category);
            // Used to generate grid later
            $this->getRequest()->setParams(['selected_products', $productIds]);
            $category->unsetData('products_position');
            $storeId = $this->getRequest()->getParam('store', 0);
            $this->_session->setLastViewedStore($storeId);
            $this->getRequest()->setControllerName('catalog_category');
            return $resultRaw->setContents(
                $this->layoutFactory->create()->createBlock(
                    'Magento\Catalog\Block\Adminhtml\Category\Tab\Product',
                    'category.product.grid'
                )->toHtml()
            );
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->getResponse()->setHttpResponseCode(403)->setBody($e->getMessage());
        }
    }
}
