<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Controller\Adminhtml\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class Count extends \Magento\Catalog\Controller\Adminhtml\Category\Grid
{

    protected $_backendSession;
    protected $_helperData;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Wyomind\DynamicCategory\Helper\Data $helperData
    ) {
    

        $this->_helperData = $helperData;
        parent::__construct($context, $resultRawFactory, $layoutFactory);
    }

    public function execute()
    {
        if (!$category = $this->_initCategory(true)) {
            return;
        }
        $storeId = $this->getRequest()->getParam('store', 0);
        $count = $this->_helperData->getCategoryProductCount($category, $storeId);
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents($count);
    }
}
