<?php

namespace Wyomind\DynamicCategory\Controller\Adminhtml\Rules;

class Import extends \Magento\Catalog\Controller\Adminhtml\Category\Grid
{

    public function execute()
    {
        if ($categoryId = $this->getRequest()->getParam('category_id')) {
            $this->getRequest()->setParam('id', $categoryId);
        }
        if (!$category = $this->_initCategory()) {
            $response = __('Specified category is not valid.');
        } else {
            $resultRaw = $this->resultRawFactory->create();
            return $resultRaw->setContents(
                $this->layoutFactory->create()->createBlock(
                    '\Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic\Conditions',
                    'category.dynamic.conditions'
                )->toHtml()
            );
        }
    }
}
