<?php

/* *
 * Copyright Â© 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshAll extends Command
{

    protected $_caegoryFactory = null;
    protected $_categoryCollectionFactory = null;
    protected $_helperIndexerFactory = null;
    protected $_state = null;

    public function __construct(
    \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Wyomind\DynamicCategory\Helper\IndexerFactory $helperIndexerFactory,
        \Magento\Framework\App\State $state
    )
    {
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_helperIndexerFactory = $helperIndexerFactory;
        $this->_state = $state;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('wyomind:dynamiccategory:refresh_all')
            ->setDescription(__('Refresh matching products for all categories'))
            ->setDefinition([]);
        parent::configure();
    }

    protected function execute(
    InputInterface $input,
        OutputInterface $output
    )
    {

        $returnValue = \Magento\Framework\Console\Cli::RETURN_FAILURE;

        try {
            try {
                $this->_state->setAreaCode('adminhtml');
            } catch (\Exception $e) {
                
            }

            $collection = $this->_categoryCollectionFactory->create()
                ->addIsActiveFilter()
                ->addAttributeToFilter('dynamic_products_conds', ['notnull' => true])
                ->addAttributeToFilter('dynamic_products_conds', ['neq' => 'a:0:{}']);
            foreach ($collection as $category) {
                $category = $this->_categoryFactory->create()->load($category->getId());
                /* @var \Wyomind\DynamicCateory\Helper\IndexerFactory */
                $this->_helperIndexerFactory->create()->process($category);
            }

            $returnValue = \Magento\Framework\Console\Cli::RETURN_SUCCESS;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $output->writeln($e->getMessage());
            $returnValue = \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }


        return $returnValue;
    }

    public function reindexCategory(\Magento\Catalog\Model\Category $category, $output)
    {
        try {
            $category = $this->_categoryFactory->create()->load($category->getId());
            $output->writeln(sprintf('Refreshing category %d', $category->getId()));
            $this->_helperIndexerFactory->create()->process($category);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $output->writeln(sprintf('Ignoring exception for category %d: %s', $category->getId(), $e->getMessage()));
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
