<?php

/*
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Observer;

class CatalogCategoryPrepareSave implements \Magento\Framework\Event\ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Mage_Catalog_Model_Category $category */
        $category = $observer->getEvent()->getCategory();
        $conds = "a:0:{}";
        $data = $observer->getEvent()->getRequest()->getPost();
        
        if (isset($data['dynamic_products_conds'])) {
            if (isset($data['dynamic_products_conds']['conditions'])) {
                if (count($data['dynamic_products_conds']['conditions']) > 1) {
                    $conds = serialize($data['dynamic_products_conds']['conditions']);
                }
            } else {
                $conds = $data['dynamic_products_conds'];
            }
        }
        
        $category->setDynamicProductsConds($conds);
        
        if ($category->getOrigData('dynamic_products_conds') == null) {
            $originalData = "a:0:{}";
        } else {
            $originalData = $category->getOrigData('dynamic_products_conds');
        }
        
        if ($category->getDynamicProductsConds() != $originalData || (isset($data['dynamic_products_conds']['force']) && $data['dynamic_products_conds']['force'] == 1)) {
            $category->setDynamicProductsRefresh(1);
            $category->unsPostedProducts();

        }

        if (empty($conds) || $conds === "a:0:{}") {
            $category->setDynamicProductsRefresh(0);
        }

        return $this;
    }

}
