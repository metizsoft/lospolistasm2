<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Model;

class Rule extends \Magento\CatalogRule\Model\Rule
{

    protected $_combineDynamicCategoryFactory = null;
    protected $_logger = null;

    public function __construct(
    \Magento\Framework\Model\Context $context,
            \Magento\Framework\Registry $registry,
            \Magento\Framework\Data\FormFactory $formFactory,
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\CatalogRule\Model\Rule\Condition\CombineFactory $combineFactory,
            \Wyomind\DynamicCategory\Model\Rule\Condition\CombineFactory $combineDynamicCategoryFactory,
            \Magento\CatalogRule\Model\Rule\Action\CollectionFactory $actionCollectionFactory,
            \Magento\Catalog\Model\ProductFactory $productFactory,
            \Magento\Framework\Model\ResourceModel\Iterator $resourceIterator,
            \Magento\Customer\Model\Session $customerSession,
            \Magento\CatalogRule\Helper\Data $catalogRuleData,
            \Magento\Framework\App\Cache\TypeListInterface $cacheTypesList,
            \Magento\Framework\Stdlib\DateTime $dateTime,
            \Magento\CatalogRule\Model\Indexer\Rule\RuleProductProcessor $ruleProductProcessor,
            \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
            \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
            array $relatedCacheTypes = [],
            array $data = []
    )
    {



        $this->_combineDynamicCategoryFactory = $combineDynamicCategoryFactory;
        parent::__construct(
                $context, $registry, $formFactory, $localeDate, $productCollectionFactory, $storeManager, $combineFactory, $actionCollectionFactory, $productFactory, $resourceIterator, $customerSession, $catalogRuleData, $cacheTypesList, $dateTime, $ruleProductProcessor, $resource, $resourceCollection, $relatedCacheTypes, $data
        );
    }

    /**
     * Store websites map for better performances when looping on very large catalog
     *
     * @var array
     */
    protected $_websitesMap;

    /**
     * Getter for rule conditions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return $this->_combineDynamicCategoryFactory->create();
    }

    /**
     * Override default method in order to cache websites mapping
     *
     * @return array
     */
    protected function _getWebsitesMap()
    {
        if (null === $this->_websitesMap) {
            foreach ($this->_storeManager->getWebsites(true) as $website) {
                /** @var $website Mage_Core_Model_Website */
                if ($website->getDefaultStore()) {
                    $this->_websitesMap[$website->getId()] = $website->getDefaultStore()->getId();
                }
            }
        }
        return $this->_websitesMap;
    }

    /**
     * Do not use resource iterator which can be slow
     *
     * @return array
     */
    public function getDynamicCategoryMatchingProductIds()
    {
        $this->_productIds = [];
        if ($this->getWebsiteIds()) {
            /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = $this->_productCollectionFactory->create();
            $collection->addWebsiteFilter($this->getWebsiteIds());
            if ($this->_productsFilter) {
                $collection->addIdFilter($this->_productsFilter);
            }
            $this->getConditions()->collectValidatedAttributes($collection);
            $rows = $this->getResource()->getConnection()
                    ->fetchAll($collection->getSelect()->distinct(false));
            $product = $this->_productFactory->create();
            $conds = $this->getConditions();
            foreach ($rows as $k => $row) {
                $product = clone $product;
                $product->setData($row);
                $results = [];
                foreach ($this->_getWebsitesMap() as $websiteId => $defaultStoreId) {
                    $product->setData('store_id', $defaultStoreId);
                    $results[$websiteId] = (int) $conds->validate($product);
                }
                $productId = $row['entity_id'];
                if (isset($this->_productIds[$productId])) {
                    $results = array_merge($this->_productIds[$productId], $results);
                }
                $this->_productIds[$productId] = $results;
            }
        }
        return $this->_productIds;
    }

    public function getProductCollectionFactory()
    {
        return $this->_productCollectionFactory;
    }

}
