<?php

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class TotalChildQty extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init("catalog_product_relation", 'id');
    }

    public function getItems()
    {
        $table = $this->getTable("catalog_product_relation");
        $select = $this->getConnection()->select()
                ->from(['relation' => $table], 'parent_id')
                ->joinLeft(
                    ['stock' => $this->getTable(\Magento\CatalogInventory\Model\Stock\Item::ENTITY)],
                    'relation.child_id = stock.product_id',
                    ['total_child_qty' => new \Zend_Db_Expr('SUM(stock.qty)')]
                )
                ->group('relation.parent_id');
        return array_map('floatval', $this->getConnection()->fetchPairs($select));
    }
}
