<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class Promo extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init("catalog_product_index_price", 'id');
    }

    public function getItems()
    {
        $table = $this->getTable('catalog_product_index_price');
        $select = $this->getConnection()->select()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->from(['price_index' => $table], 'entity_id')
                ->group('entity_id')
                ->having(new \Zend_Db_Expr('SUM(final_price < price) > 0'));
        return array_flip($this->getConnection()->fetchCol($select));
    }
}
