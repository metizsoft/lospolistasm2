<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class HasImage extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init("catalog_product_entity", 'id');
    }

    public function getItems()
    {
        $table = $this->getTable('catalog_product_entity');
        $select = $this->getConnection()->select()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->from(['e' => $table], 'entity_id')
                ->joinLeft(
                    ['mge' => $this->getTable(\Magento\Catalog\Model\ResourceModel\Product\Gallery::GALLERY_VALUE_TO_ENTITY_TABLE)],
                    'e.entity_id = mge.entity_id'
                )
                ->joinLeft(
                    ['mg' => $this->getTable(\Magento\Catalog\Model\ResourceModel\Product\Gallery::GALLERY_TABLE)],
                    'mge.value_id = mg.value_id'
                )
                ->where('mg.value IS NULL');
        return array_flip($this->getConnection()->fetchCol($select));
    }
}
