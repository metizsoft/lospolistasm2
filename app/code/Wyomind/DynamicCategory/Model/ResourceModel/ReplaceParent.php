<?php

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class ReplaceParent extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init("catalog_product_relation", 'product_id');
    }

    public function getItems($ids = [])
    {
        $table = $this->getTable('catalog_product_relation');
        $parentIds = $this->getConnection()->fetchPairs(
            $this->getConnection()->select()
                        ->from($table, ['child_id', 'parent_id'])
                        ->where('child_id IN (?)', $ids)
        );
        return array_map('intval', $parentIds);
    }
}
