<?php

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class Salable extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init("cataloginventory_stock_status", 'product_id');
    }

    public function getItems()
    {
        $table = $this->getTable("cataloginventory_stock_status");
        $fields = ["product_id", "is_in_stock"];
        $select = $this->getConnection()->select()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->from($table, ['product_id', 'stock_status'])
                ->where('stock_status = 1');
        return $this->getConnection()->fetchPairs($select);
    }
}
