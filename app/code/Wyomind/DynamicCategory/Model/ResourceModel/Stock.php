<?php

namespace Wyomind\DynamicCategory\Model\ResourceModel;

class Stock extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init(\Magento\CatalogInventory\Model\Stock::ENTITY, 'id');
    }

    public function getItems()
    {
        $table = $this->getTable(\Magento\CatalogInventory\Model\Stock\Item::ENTITY);
        $fields = ["product_id", "is_in_stock"];
        $select = $this->getConnection()->select()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->from($table, ['product_id', 'is_in_stock'])
                ->where("is_in_stock=1");
        return $this->getConnection()->fetchPairs($select);
    }
}
