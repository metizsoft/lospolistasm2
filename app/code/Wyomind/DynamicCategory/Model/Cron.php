<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Model;

class Cron
{

    private $_coreHelper;
    private $_categoryCollectionFactory;
    private $_dateTime;
    private $_categoryFactory;
    private $_helperIndexer;
    private $_logger;
    private $_magelogger;

    public function __construct(
        \Wyomind\Core\Helper\Data $coreHelper,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Wyomind\DynamicCategory\Helper\Indexer $helperIndexer,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Wyomind\DynamicCategory\Logger\Logger $logger,
        \Psr\Log\LoggerInterface $magelogger
    ) {
    

        $this->_categoryFactory = $categoryFactory;
        $this->_coreHelper = $coreHelper;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_dateTime = $dateTime;
        $this->_helperIndexer = $helperIndexer;
        $this->_logger = $logger;
        $this->_magelogger = $magelogger;
    }

    public function reindexAll(\Magento\Cron\Model\Schedule $schedule, $force = false)
    {
        if ($force || $this->_coreHelper->getStoreConfig('dynamiccategory/general/enable_reindex')) {
            $delay = (int) $this->_coreHelper->getStoreConfig('dynamiccategory/general/reindex_delay');
            $collection = $this->_categoryCollectionFactory->create()
                    ->addIsActiveFilter()
                    ->addAttributeToFilter('dynamic_products_conds', ['notnull' => true])
                    ->addAttributeToFilter('dynamic_products_conds', ['neq' => 'a:0:{}'])
                    ->addFieldToFilter('updated_at', [
                'lteq' => $this->_dateTime->date(\Magento\Framework\Stdlib\DateTime::DATE_PHP_FORMAT . " h:i:s", $this->_dateTime->timeStamp() - ($delay * 60 * 60))
                    ]);
            foreach ($collection as $category) {
                $this->reindexCategory($category);
            }
        }
    }

    /**
     * Reindex a single category
     *
     * @param Mage_Catalog_Model_Category $category
     */
    public function reindexCategory(\Magento\Catalog\Model\Category $category)
    {
        try {
            $category = $this->_categoryFactory->create()->load($category->getId()); // Needed to avoid bug with empty URL key and Include in Menu switched
            $this->_helperIndexer->process($category);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->_logger->notice(sprintf(
                '[Dynamic Category] Ignoring exception for category %d: %s',
                $category->getId(),
                $e->getMessage()
            ));
        } catch (\Exception $e) {
            $this->_magelogger->warn($e->getMessage());
            throw $e;
        }
    }
}
