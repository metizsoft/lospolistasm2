<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition;

class Combine extends \Magento\CatalogRule\Model\Rule\Condition\Combine
{

    protected $_value;
    protected $_conditionProductFactory;
    protected $elementName = "dynamic_products_conds";
    protected $formName = "category_form";

    public function getFormName()
    {
        return $this->formName;
    }

    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Magento\CatalogRule\Model\Rule\Condition\ProductFactory $conditionFactory,
        \Wyomind\DynamicCategory\Model\Rule\Condition\Product $conditionProductFactory,
        array $data = []
    ) {
    


        $this->_conditionProductFactory = $conditionProductFactory;
        parent::__construct($context, $conditionFactory, $data);
        $this->setType('Wyomind\DynamicCategory\Model\Rule\Condition\Combine');
    }

    public function asHtmlRecursive()
    {
        $html = $this->asHtml() . '<ul id="' . $this->getPrefix() . '__' . $this->getId() . '__children" class="rule-param-children">';
        foreach ($this->getConditions() as $cond) {
            /** @var Mage_Rule_Model_Condition_Abstract $cond */
            try {
                $html .= '<li>' . $cond->asHtmlRecursive() . '</li>';
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $html .= sprintf(
                    '<li>%s&nbsp;<span class="error">%s</span>%s</li>',
                    $cond->getAttributeName(),
                    $e->getMessage(),
                    $cond->getRemoveLinkHtml()
                );
            }
        }
        $html .= '<li>' . $this->getNewChildElement()->getHtml() . '</li></ul>';
        return $html;
    }

    public function getNewChildSelectOptions()
    {
        $productAttributes = $this->_conditionProductFactory->loadAttributeOptions()->getAttributeOption();
        $attributes = [];
        foreach ($productAttributes as $code => $label) {
            $attributes[] = ['value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product|' . $code, 'label' => $label];
        }
        $conditions = [
            [
                "value" => null,
                "label" => __("Please choose a condition to add")
            ]
        ];
        $conditions = array_merge_recursive($conditions, [
            [
                'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Combine',
                'label' => __('Conditions Combination'),
            ],
            [
                'label' => __('Special Product Condition'),
                'value' => [
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Salable|is_salable',
                        'label' => __('Is Salable'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Promo|in_promo',
                        'label' => __('In Promo'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\IsNew|is_new',
                        'label' => __('Is New'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Created|created_at',
                        'label' => __('Created'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Quantity|quantity',
                        'label' => __('Quantity In Stock'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Stock|stock',
                        'label' => __('In Stock'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Price\Special\Applied|price_special_applied',
                        'label' => __('Special Price Applied'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\Type|product_type',
                        'label' => __('Product Type'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\HasImage|has_image',
                        'label' => __('Has Image'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\TotalChildQty|total_child_products_qty',
                        'label' => __('Total Child Products Quantity In Stock'),
                    ],
                    [
                        'value' => 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\ReplaceParent|product_parent',
                        'label' => __('Replace Matching Simple Products By Parent Products'),
                    ],
                ],
            ],
            [
                'value' => $attributes,
                'label' => __('Product Attribute'),
            ],
        ]);
        return $conditions;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $conds = $this->getConditions();
        if (!$conds) {
            return true;
        }
        $all = $this->getAggregator() === 'all';
        $true = (bool) $this->getValue();
        foreach ($conds as $cond) {
            $validated = $cond->validate($model);
            if ($all && $validated !== $true) {
                return false;
            } elseif (!$all && $validated === $true) {
                return true;
            }
        }
        return $all ? true : false;
    }

    public function getValue()
    {
        if (null === $this->_value) {
            $this->_value = parent::getValue();
        }
        return $this->_value;
    }

    public function getPrefix()
    {
        return $this->_getData('prefix');
    }

    public function getConditions()
    {
        $prefix = $this->getPrefix();
        return $this->_getData($prefix ? $prefix : 'conditions');
    }

    public function getAggregator()
    {
        return $this->_getData('aggregator');
    }
}
