<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition;

class Product extends \Magento\CatalogRule\Model\Rule\Condition\Product
{

    protected $_attribute;
    protected $_objectResource;
    protected $_op;
    protected $_isArrayOperatorType;
    protected $elementName = "dynamic_products_conds";
    protected $formName = "category_form";
    protected $_resourceModelStock;
    protected $_resourceModelSalable;
    protected $_productTypeFactory;
    protected $_resourceModelQuantity;
    protected $_dateTime;
    protected $_productCollectionFactory;
    protected $_resourceModelPromo;
    protected $_resourceModelHasImage;
    protected $_resourceModelTotalChildQty;
    protected $_hasImage;

    public function __construct(
    \Magento\Rule\Model\Condition\Context $context, \Magento\Backend\Helper\Data $backendData,
            \Magento\Eav\Model\Config $config,
            \Magento\Catalog\Model\ProductFactory $productFactory,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
            \Magento\Catalog\Model\ResourceModel\Product $productResource,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attrSetCollection,
            \Magento\Framework\Locale\FormatInterface $localeFormat,
            \Wyomind\DynamicCategory\Model\ResourceModel\Stock $resourceModelStock,
            \Wyomind\DynamicCategory\Model\ResourceModel\Salable $resourceModelSalable,
            \Wyomind\DynamicCategory\Model\ResourceModel\Quantity $resourceModelQuantity,
            \Wyomind\DynamicCategory\Model\ResourceModel\Promo $resourceModelPromo,
            \Wyomind\DynamicCategory\Model\ResourceModel\HasImage $resourceModelHasImage,
            \Wyomind\DynamicCategory\Model\ResourceModel\TotalChildQty $resourceModelTotalChildQty,
            \Magento\Catalog\Model\Product\TypeFactory $productTypeFactory,
            \Magento\Framework\Stdlib\DateTime\DateTime $dateTime, array $data = []
    )
    {



        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_resourceModelStock = $resourceModelStock;
        $this->_productTypeFactory = $productTypeFactory;
        $this->_resourceModelSalable = $resourceModelSalable;
        $this->_resourceModelQuantity = $resourceModelQuantity;
        $this->_resourceModelPromo = $resourceModelPromo;
        $this->_resourceModelHasImage = $resourceModelHasImage;
        $this->_resourceModelTotalChildQty = $resourceModelTotalChildQty;
        $this->_dateTime = $dateTime;
        parent::__construct($context, $backendData, $config, $productFactory, $productRepository, $productResource, $attrSetCollection, $localeFormat, $data);
    }

    public function getFormName()
    {
        return $this->formName;
    }

    /**
     * Get input type directly for performance optimization
     *
     * @return string
     */
    public function getInputType()
    {
        if (null === $this->_inputType) {
            $this->_inputType = parent::getInputType();
        }
        return $this->_inputType;
    }

    /**
     * Get correct operator for validation
     *
     * @return string
     */
    public function getOperatorForValidate()
    {
        if (null === $this->_op) {
            $this->_op = parent::getOperatorForValidate();
        }
        return $this->_op;
    }

    /**
     * @return string
     */
    public function getAttribute()
    {
        if (null === $this->_attribute) {
            $this->_attribute = $this->_getData('attribute');
        }
        return $this->_attribute;
    }

    /**
     * @param $object
     * @return Mage_Catalog_Model_Resource_Abstract
     */
    public function getObjectResource($object)
    {
        if (null === $this->_objectResource) {
            $this->_objectResource = $object->getResource();
        }
        return $this->_objectResource;
    }

    /**
     * Check if value should be array
     *
     * Depends on operator input type
     *
     * @return bool
     */
    public function isArrayOperatorType()
    {
        if (null === $this->_isArrayOperatorType) {
            $this->_isArrayOperatorType = parent::isArrayOperatorType();
        }
        return $this->_isArrayOperatorType;
    }

    /**
     * Load attribute options but without the need to be flagged as "Use for Promo Rule Conditions"
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $productAttributes = $this->_productResource
                ->loadAllAttributes()
                ->getAttributesByCode();
        $attributes = [];
        foreach ($productAttributes as $attribute) {
            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }
        $this->_addSpecialAttributes($attributes);
        asort($attributes);
        $this->setAttributeOption($attributes);
        return $this;
    }

    /**
     * Default operator input by type map getter
     *
     * @return array
     */
    public function getDefaultOperatorInputByType()
    {
        parent::getDefaultOperatorInputByType();
        // Matches regexp
        $this->_defaultOperatorInputByType['string'][] = '^$';
        $this->_defaultOperatorInputByType['numeric'][] = '^$';
        $this->_defaultOperatorInputByType['date'][] = '^$';
        // Does not match regexp
        $this->_defaultOperatorInputByType['string'][] = '!^$';
        $this->_defaultOperatorInputByType['numeric'][] = '!^$';
        $this->_defaultOperatorInputByType['date'][] = '!^$';
        return $this->_defaultOperatorInputByType;
    }

    /**
     * Default operator options getter
     * Provides all possible operator options
     *
     * @return array
     */
    public function getDefaultOperatorOptions()
    {
        $options = parent::getDefaultOperatorOptions();
        $options['^$'] = __('matches regexp');
        $options['!^$'] = __('does not match regexp');
        return $options;
    }

    /**
     * Retrieve parsed value
     *
     * @return array|string|int|float
     */
    public function getValueParsed()
    {
        if (!$this->getDataSetDefault('value_parsed', null)) {
            $value = $this->_getData('value');
            if ($this->isArrayOperatorType() && is_string($value)) {
                $value = preg_split('#\s*[,;]\s*#', $value, null, PREG_SPLIT_NO_EMPTY);
            }
            $this->setData('value_parsed', $value);
        }
        return $this->_getData('value_parsed');
    }

    /**
     * Validate product attribute value for condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $attrCode = $this->getAttribute();
        if ('category_ids' == $attrCode) {
            return $this->validateAttribute($object->getCategoryIds());
        }
        if ('attribute_set_id' == $attrCode) {
            return $this->validateAttribute($object->getData($attrCode));
        }
        $oldAttrValue = $object->getData($attrCode);
        if ($oldAttrValue === null) {
            return false;
        }
            
        $this->_setAttributeValue($object); 

        $result = $this->validateAttribute($object->getData($attrCode));
        $this->_restoreOldAttrValue($object, $oldAttrValue);

        return (bool) $result;
    }

  



    /**
     * Validate product attribute value for condition
     *
     * @param   mixed $validatedValue product attribute value
     * @return  bool
     */
    public function validateAttribute($validatedValue)
    {
        if (is_object($validatedValue)) {
            return false;
        }
        /**
         * Condition attribute value
         */
        $value = $this->getValueParsed();
        /**
         * Comparison operator
         */
        $op = $this->getOperatorForValidate();
        // if operator requires array and it is not, or on opposite, return false
        if ($this->isArrayOperatorType() xor is_array($value)) {
            return false;
        }
        if ($op === '^$' || $op === '!^$') {
            $result = (bool) preg_match($value, $validatedValue);
            return $op === '!^$' ? !$result : $result;
        }
        $result = false;
        switch ($op) {
            case '==':
            case '!=':
                if (is_array($value)) {
                    if (is_array($validatedValue)) {
                        $result = array_intersect($value, $validatedValue);
                        $result = !empty($result);
                    } else {
                        return false;
                    }
                } else {
                    if (is_array($validatedValue)) {
                        $result = count($validatedValue) == 1 && array_shift($validatedValue) == $value;
                    } else {
                        $result = $this->_compareValues($validatedValue, $value);
                    }
                }
                break;
            case '<=':
            case '>':
                if (!is_scalar($validatedValue)) {
                    return false;
                } else {
                    $result = $validatedValue <= $value;
                }
                break;
            case '>=':
            case '<':
                if (!is_scalar($validatedValue)) {
                    return false;
                } else {
                    $result = $validatedValue >= $value;
                }
                break;
            case '{}':
            case '!{}':
                if (is_scalar($validatedValue) && is_array($value)) {
                    foreach ($value as $item) {
                        if (stripos($validatedValue, $item) !== false) {
                            $result = true;
                            break;
                        }
                    }
                } elseif (is_array($value)) {
                    if (is_array($validatedValue)) {
                        $result = array_intersect($value, $validatedValue);
                        $result = !empty($result);
                    } else {
                        return false;
                    }
                } else {
                    if (is_array($validatedValue)) {
                        $result = in_array($value, $validatedValue);
                    } else {
                        $result = $this->_compareValues($value, $validatedValue, false);
                    }
                }
                break;
            case '()':
            case '!()':
                if (is_array($validatedValue)) {
                    $result = count(array_intersect($validatedValue, (array) $value)) > 0;
                } else {
                    $value = (array) $value;
                    foreach ($value as $item) {
                        if ($this->_compareValues($validatedValue, $item)) {
                            $result = true;
                            break;
                        }
                    }
                }
                break;
        }
        if ('!=' == $op || '>' == $op || '<' == $op || '!{}' == $op || '!()' == $op) {
            $result = !$result;
        }
        return $result;
    }

    /**
     * Get attribute value
     *
     * @param Varien_Object $object
     * @return mixed
     */
    protected function _getAttributeValue(\Magento\Framework\Model\AbstractModel $object)
    {
        $attrCode = $this->getAttribute();
        $storeId = $object->getDataSetDefault('store_id', null);
        $defaultStoreId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;

        $entityId = $object->getDataSetDefault('entity_id', null);
        $productValues = isset($this->_entityAttributeValues[$entityId]) ? $this->_entityAttributeValues[$entityId] : [];
        $defaultValue = isset($productValues[$defaultStoreId]) ? $productValues[$defaultStoreId] : $object->getDataSetDefault($attrCode, null);
       
        $value = isset($productValues[$storeId]) ? $productValues[$storeId] : $defaultValue;
        $value = $this->_prepareDatetimeValue($value, $object);
        $value = $this->_prepareMultiselectValue($value, $object);
        return $value;
    }

    /**
     * Prepare datetime attribute value
     *
     * @param mixed $value
     * @param Varien_Object $object
     * @return mixed
     */
    protected function _prepareDatetimeValue($value, \Magento\Framework\Model\AbstractModel $object)
    {
        $resource = $this->getObjectResource($object);
        $attribute = $resource->getAttribute($this->getAttribute());
        if ($attribute && $attribute->getDataSetDefault('backend_type', null) == 'datetime') {
            $value = strtotime($value);
        }
        return $value;
    }

    /**
     * Prepare multiselect attribute value
     *
     * @param mixed $value
     * @param Varien_Object $object
     * @return mixed
     */
    protected function _prepareMultiselectValue(
    $value, \Magento\Framework\Model\AbstractModel $object
    )
    {

        $resource = $this->getObjectResource($object);
        $attribute = $resource->getAttribute($this->getAttribute());
        if ($attribute && $attribute->getDataSetDefault('frontend_input', null) == 'multiselect') {
            $value = strlen($value) ? explode(',', $value) : [];
        }
        return $value;
    }
    
    protected function _validateProduct(\Magento\Framework\Model\AbstractModel $object)
    {
        return \Magento\Rule\Model\Condition\AbstractCondition::validate($object);
    }

}
