<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Stock extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product\Boolean
{

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_stock');
        $this->setValueName('In Stock');
    }

    public function getAttributeName()
    {
        return __('In Stock');
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelStock->getItems();
        return $this;
    }

}
