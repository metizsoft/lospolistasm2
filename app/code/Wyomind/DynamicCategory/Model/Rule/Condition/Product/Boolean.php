<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

abstract class Boolean extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'boolean';

    public function getValueElementType()
    {
        return 'select';
    }

    public function getValue()
    {
        $value = parent::getValue();
        return null !== $value ? $value : 1;
    }

    public function getValueSelectOptions()
    {
        return [
            ['value' => 1, 'label' => __('Yes')],
            ['value' => 0, 'label' => __('No')],
        ];
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        if (isset($this->_entityAttributeValues[$object->_getData('entity_id')])) {
            $object->setData($this->_getData('attribute'), $this->_entityAttributeValues[$object->_getData('entity_id')]);
        }
        return (bool) $this->_validateProduct($object);
    }

}
