<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class TotalChildQty extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'numeric';

    protected function _construct()
    {
        parent::_construct();
        $this->setType('total_child_products_qty');
        $this->setValueName('Total Child Products Quantity In Stock');
    }

    public function getAttributeName()
    {
        return __('Total Child Products Quantity In Stock');
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelTotalChildQty->getItems();
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $qty = 0;
        if (isset($this->_entityAttributeValues[$object->_getData('entity_id')])) {
            $qty = $this->_entityAttributeValues[$object->_getData('entity_id')];
        }
        $object->setData($this->_getData('attribute'), $qty);
        return $this->_validateProduct($object);
    }

}
