<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Created extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'string';

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_created');
        $this->setValueName('Created');
    }

    public function getValueElementType()
    {
        return 'text';
    }

    public function getInputType()
    {
        return $this->_inputType;
    }

    public function getAttributeName()
    {
        return __('Created');
    }

    public function getDefaultOperatorOptions()
    {
        if (null === $this->_defaultOperatorOptions) {
            $this->_defaultOperatorOptions = [
                '<=' => __('during the last'),
            ];
        }
        return $this->_defaultOperatorOptions;
    }

    public function asHtml()
    {
        $html = $this->getTypeElementHtml()
                . $this->getAttributeElementHtml()
                . $this->getOperatorElementHtml()
                . $this->getValueElementHtml()
                . __('days')
                . $this->getRemoveLinkHtml()
                . $this->getChooserContainerHtml();
        return $html;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $createdAt = $this->_dateTime->timestamp($object->getCreatedAt(), \Zend_Date::ISO_8601);
        $now = $this->_dateTime->timestamp();
        $diff = $now - $createdAt;
        $days = floor($diff / 60 / 60 / 24);
        return $days <= $this->getValue();
    }

}
