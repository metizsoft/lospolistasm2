<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Type extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'multiselect';

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_type');
        $this->setValueName('Product Type');
    }

    public function getValueElementType()
    {
        return 'multiselect';
    }

    public function getAttributeName()
    {
        return __('Product Type');
    }

    public function getDefaultOperatorOptions()
    {
        return [
            '()' => __('is one of'),
            '!()' => __('is not one of')
        ];
    }

    public function getValueSelectOptions()
    {
        $types = $this->_productTypeFactory->create()->getTypes();
        $options = [];
        foreach ($types as $type) {
            $options[] = ["value" => $type["name"], "label" => $type["label"]];
        }
        return $options;
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $result = in_array($object->getTypeId(), $this->_getData('value'));
        if ($this->getOperatorForValidate() == "!()") {
            return !$result;
        };
        return $result;
    }

}
