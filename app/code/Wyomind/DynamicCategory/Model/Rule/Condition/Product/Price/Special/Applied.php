<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product\Price\Special;

class Applied extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product\Boolean
{

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_special_price_applied');
        $this->setValueName('Special Price Applied');
    }

    public function getAttributeName()
    {
        return __('Special Price Applied');
    }

    public function getDefaultOperatorOptions()
    {
        return ['==' => __('is')];
    }

    public function getValueSelectOptions()
    {
        return [
            ['value' => 1, 'label' => __('Yes')],
        ];
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $todayStartOfDayDate = $this->_dateTime->date(\Magento\Framework\Stdlib\DateTime::DATE_PHP_FORMAT . " 00:00:00");
        $todayEndOfDayDate = $this->_dateTime->date(\Magento\Framework\Stdlib\DateTime::DATE_PHP_FORMAT . " 23:59:59");
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $this->_productCollectionFactory->create()->addAttributeToFilter('special_from_date', ['or' => [
                        0 => ['date' => true, 'to' => $todayEndOfDayDate],
                        1 => ['is' => new \Zend_Db_Expr('null')]]
                        ], 'left')
                ->addAttributeToFilter('special_to_date', ['or' => [
                        0 => ['date' => true, 'from' => $todayStartOfDayDate],
                        1 => ['is' => new \Zend_Db_Expr('null')]]
                        ], 'left')
                ->addAttributeToFilter(
                [
                    ['attribute' => 'special_from_date', 'is' => new \Zend_Db_Expr('not null')],
                    ['attribute' => 'special_to_date', 'is' => new \Zend_Db_Expr('not null')]
                ]
        );
        $this->_entityAttributeValues = array_flip($collection->getAllIds());
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        return isset($this->_entityAttributeValues[$object->_getData('entity_id')]);
    }

}
