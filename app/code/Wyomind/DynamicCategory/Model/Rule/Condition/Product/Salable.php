<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Salable extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product\Boolean
{

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_is_salable');
        $this->setValueName('Is Salable');
    }

    public function getAttributeName()
    {
        return __('Is Salable');
    }

    public function getDefaultOperatorOptions()
    {
        return ['==' => __('is')];
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelSalable->getItems();
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $isSalable = isset($this->_entityAttributeValues[$object->_getData('entity_id')]);
        return $this->getValue() ? $isSalable : !$isSalable;
    }

}
