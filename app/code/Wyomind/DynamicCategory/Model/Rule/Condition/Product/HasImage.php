<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class HasImage extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product\Boolean
{

    protected function _construct()
    {
        parent::_construct();
        $this->setType('has_image');
        $this->setValueName('Has Image');
    }

    public function getAttributeName()
    {
        return __('Has Image');
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelHasImage->getItems();
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $hasImage = !isset($this->_entityAttributeValues[$object->_getData('entity_id')]);
        return $this->getValue() ? $hasImage : !$hasImage;
    }

}
