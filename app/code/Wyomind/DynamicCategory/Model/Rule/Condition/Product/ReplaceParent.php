<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class ReplaceParent extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'select';

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_parent');
        $this->setValueName('Replace Matching Simple Products By Parent Products');
    }

    public function getValueElementType()
    {
        return 'select';
    }

    public function getAttributeName()
    {
        return __('Replace Matching Simple Products By Parent Products');
    }

    public function getDefaultOperatorOptions()
    {
        return ['==' => __('and')];
    }

    public function getValueSelectOptions()
    {
        return [
            ['value' => 'keep_orphans', 'label' => __('Keep Orphans')],
            ['value' => 'remove_orphans', 'label' => __('Discard Orphans')],
        ];
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        return true; // Fake condition, filtering will be processed in global product collection
    }

}
