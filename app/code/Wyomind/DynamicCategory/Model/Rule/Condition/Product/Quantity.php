<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Quantity extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product
{

    protected $_inputType = 'numeric';

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_quantity');
        $this->setValueName('Quantity In Stock');
    }

    public function getAttributeName()
    {
        return __('Quantity In Stock');
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelQuantity->getItems();
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        if (isset($this->_entityAttributeValues[$object->_getData('entity_id')])) {
            $object->setData($this->_getData('attribute'), $this->_entityAttributeValues[$object->_getData('entity_id')]);
        }
        return $this->_validateProduct($object);
    }

}
