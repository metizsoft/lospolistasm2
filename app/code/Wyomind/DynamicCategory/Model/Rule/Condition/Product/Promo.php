<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     2.5.0
 * @copyright   Copyright (c) 2016 Wyomind (https://www.wyomind.com/)
 */

namespace Wyomind\DynamicCategory\Model\Rule\Condition\Product;

class Promo extends \Wyomind\DynamicCategory\Model\Rule\Condition\Product\Boolean
{

    protected function _construct()
    {
        parent::_construct();
        $this->setType('product_in_promo');
        $this->setValueName('In Promo');
    }

    public function getAttributeName()
    {
        return __('In Promo');
    }

    public function getDefaultOperatorOptions()
    {
        return ['==' => __('is')];
    }

    public function getValueSelectOptions()
    {
        return [
            ['value' => 1, 'label' => __('Yes')],
            ['value' => 0, 'label' => __('No')],
        ];
    }

    public function collectValidatedAttributes($productCollection)
    {
        unset($productCollection);
        $this->_entityAttributeValues = $this->_resourceModelPromo->getItems();
        return $this;
    }

    public function validate(\Magento\Framework\Model\AbstractModel $object)
    {
        $isPromo = isset($this->_entityAttributeValues[$object->_getData('entity_id')]);
        return $this->getValue() ? $isPromo : !$isPromo;
    }

}
