<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category;

class AssignProducts extends \Magento\Catalog\Block\Adminhtml\Category\AssignProducts
{

    protected $_template = 'Wyomind_DynamicCategory::catalog/category/edit/assign_products.phtml';
    protected $blockImport;
    protected $blockConditions;
    protected $refreshButton;

    public function getBlockImport()
    {
        if (null === $this->blockImport) {
            $this->blockImport = $this->getLayout()->createBlock(
                'Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic\Conditions\Import',
                'category.product.conditions.import'
            );
        }
        return $this->blockImport;
    }

    public function getBlockConditions()
    {
        if (null === $this->blockConditions) {
            $this->blockConditions = $this->getLayout()->createBlock(
                'Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic\Conditions',
                'category.product.conditions'
            );
        }
        return $this->blockConditions;
    }

    public function getBlockRefreshButton()
    {
        if (null === $this->refreshButton) {
            $this->refreshButton = $this->getLayout()->createBlock(
                'Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic\RefreshButton',
                'category.product.refresh.button'
            );
        }
        return $this->refreshButton;
    }

    public function getImportHtml()
    {
        return $this->getBlockImport()->toHtml();
    }

    public function getConditionsHtml()
    {
        return $this->getBlockConditions()->toHtml();
    }

    public function getRefreshButtonHtml()
    {
        return $this->getBlockRefreshButton()->toHtml();
    }
}
