<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic;

class RefreshButton extends \Magento\Backend\Block\Widget\Button
{

    protected function _construct()
    {
        parent::_construct();
        $this->setData(
            [
                    'id' => 'dynamic_category_refresh_btn',
                    'name' => "dynamic_category_refresh_btn",
                    'title' => __("Refresh matching products"),
                    'class' => 'save primary',
                    'onclick' => "jQuery('#force_refresh_products').val(1); jQuery('#save').trigger('click');",
                    'label' => __("Refresh matching products"),
                    "role" => "save"
                ]
        );
    }
}
