<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic;

class Conditions extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_template = 'Wyomind_DynamicCategory::catalog/category/edit/dynamic/conditions.phtml';
    protected $_dynamicCategoryRule = null;
    protected $_formFactory = null;
    protected $_fieldsetFactory = null;
    protected $_blockConditionFactory;
    protected $_unserialize;

    protected function _construct()
    {
        parent::_construct();
        $this->setId('dynamic_category_conditions');
        $this->setTitle(__('Dynamic Category Conditions Block'));
    }

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Wyomind\DynamicCategory\Model\Rule $dynamicCategoryRule,
        \Magento\Backend\Block\Widget\Form\Renderer\FieldsetFactory $fieldsetFactory,
        \Magento\Rule\Block\ConditionsFactory $blockConditionFactory,
        \Magento\Framework\Unserialize\Unserialize $unserialize,
        array $data = []
    ) {
    

        $this->_dynamicCategoryRule = $dynamicCategoryRule;
        $this->_fieldsetFactory = $fieldsetFactory;
        $this->_blockConditionFactory = $blockConditionFactory;
        $this->_unserialize = $unserialize;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Retrieve the current selected category in the admin view.
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return $this->_coreRegistry->registry('category');
    }

    /**
     * Creates the form for the condition based selection of product attributes.
     *
     * @return $this
     */
    public function _prepareForm()
    {
        $rootId = \Magento\Catalog\Model\Category::TREE_ROOT_ID;
        if ($this->_coreRegistry->registry('category')->getId() == $rootId) {
            return;
        }
        
        $conditions = $this->getCategory()->getDynamicProductsConds();
        if (is_array($conditions)) {
            $data = $conditions;
        } else {
            if ($this->getCategory()->getDynamicProductsConds()) {
                $data = ['conditions' => $this->_unserialize->unserialize($this->getCategory()->getDynamicProductsConds())];
            } else {
                $data = ['conditions' => []];
            }
        }
        
        $this->_dynamicCategoryRule->loadPost($data);
        $this->_dynamicCategoryRule->getConditions()->setJsFormObject('category_product_conditions_fieldset');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setHtmlIdPrefix('category_product_');
        $newChildUrl = $this->getUrl('catalog_rule/promo_catalog/newConditionHtml/form/category_product_conditions_fieldset');
        // Fieldset renderer
        $renderer = $this->_fieldsetFactory->create()->setTemplate('Wyomind_DynamicCategory::catalog/category/edit/dynamic/promo/fieldset.phtml')
                ->setNewChildUrl($newChildUrl);
        // Add new fieldset
        $fieldset = $form->addFieldset(
            'conditions_fieldset',
            ['legend' => __('Product Category Rules')]
        )->setRenderer($renderer);
        $conditions = $this->_blockConditionFactory->create();
        $fieldset->addField(
            'dynamic_products_conds',
            'text',
            [
                'label' => __('Conditions'),
                'title' => __('Conditions'),
                'name' => 'dynamic_products_conds',
            "data-form-part" => "category_form",
            ]
        )->setRule($this->_dynamicCategoryRule)->setRenderer($conditions);
        $fieldset->addField(
            'dynamic_products_refresh',
            'hidden',
            [
                'name' => 'dynamic_products_refresh',
            "data-form-part" => "category_form",
            ]
        );
        $form->setValues($this->getCategory()->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Retrieve category dynamic products grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('dynamiccategory/product/grid', ['_current' => true]);
    }

    /**
     * Retrieve category product count URL
     *
     * @return string
     */
    public function getProductCountUrl()
    {
        return $this->getUrl('dynamiccategory/product/count', ['_current' => true]);
    }
}
