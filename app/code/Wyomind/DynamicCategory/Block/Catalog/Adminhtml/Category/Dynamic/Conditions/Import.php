<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Block\Catalog\Adminhtml\Category\Dynamic\Conditions;

class Import extends \Magento\Backend\Block\Template
{

    /**
     * Category Model
     *
     * @var Mage_Catalog_Model_Category Category
     */
    protected $_category;
    protected $_categoryRepository;
    protected $_template = 'Wyomind_DynamicCategory::catalog/category/edit/dynamic/conditions/import.phtml';
    protected $_unserialize;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Unserialize\Unserialize $unserialize,
        array $data = []
    ) {
    

        $this->registry = $registry;
        $this->jsonEncoder = $jsonEncoder;
        $this->_categoryRepository = $categoryRepository;
        $this->_unserialize = $unserialize;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve the current selected category in the admin view.
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        if (!$this->_category) {
            $this->_category = $this->registry('category');
        }
        return $this->_category;
    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @param bool $addEmpty
     * @return array
     */
    public function getCategoryOptions($category = null, $addEmpty = true)
    {
        $options = [];
        if ($addEmpty) {
            $options[] = [
                'label' => __('-- Please Select a Category --'),
                'value' => ''
            ];
        }
        if (!$category) {
            $category = $this->_categoryRepository->get(\Magento\Catalog\Model\Category::TREE_ROOT_ID);
        }
        $conds = $category->getDynamicProductsConds();
        if (is_string($conds)) {
            $conds = $this->_unserialize->unserialize($conds);
        }
        if ($category->getLevel() > 0) {
            $label = trim(str_repeat('--', $category->getLevel() - 1) . ' ' . $category->getName());
            $options[] = [
                'value' => $category->getId(),
                'label' => $label,
                'params' => empty($conds) ? ['disabled' => 'disabled'] : [],
            ];
        }
        /** @var Mage_Catalog_Model_Resource_Category_Collection $children */
        $children = $category->getCollection();
        $children->joinUrlRewrite()
                ->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('all_children')
                ->addAttributeToSelect('is_anchor')
                ->addAttributeToSelect('dynamic_products_conds')
                ->setOrder('position', \Zend_Db_Select::SQL_ASC)
                ->addFieldToFilter('parent_id', $category->getId());
        foreach ($children as $child) {
            /** @var Mage_Catalog_Model_Category $child */
            $options = array_merge($options, $this->getCategoryOptions($child, false));
        }
        return $options;
    }

    /**
     * @return string
     */
    public function getCategorySelectHtml()
    {
        $options = $this->getCategoryOptions();
        $html = "<select id='import_conditions_field' name='import_conditions' class='select admin__control-select'>";
        foreach ($options as $option) {
            $params = [];
            if (isset($option["params"])) {
                foreach ($option["params"] as $key => $value) {
                    $params[] = $key . "='" . $value . "'";
                }
            }
            $html.="<option value='" . $option["value"] . "' " . implode(" ", $params) . ">" . $option["label"] . "</option>";
        }
        $html.="</select>";
        return $html;
    }

    /**
     * @return string
     */
    public function getImportCondsUrl()
    {
        return $this->getUrl('dynamiccategory/rules/import', ['_current' => true, 'store' => null]);
    }
}
