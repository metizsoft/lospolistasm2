<?php

namespace Wyomind\DynamicCategory\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{

    public $fileName = '/var/log/DynamicCategory.log';
    public $loggerType = \Monolog\Logger::NOTICE;
}
