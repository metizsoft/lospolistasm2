<?php

/**
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Install Data needed for Simple Google Shopping
 */
class InstallData implements InstallDataInterface
{

    /** @var EavSetupFactory $eavSetupFactory */
    private $eavSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(
    ModuleDataSetupInterface $setup,
            ModuleContextInterface $context
    )
    {
        
        unset($context);
        
        $setup->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY, 'dynamic_products_conds', [
            'group' => 'General Information',
            'label' => '',
            'type' => 'text',
            'input' => '',
            'default' => '',
            'note' => '',
            'frontend' => '',
            'source' => '',
            'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
            'visible' => false,
            'required' => false,
            'user_defined' => false,
            'visible_on_front' => false,
            'unique' => false
                ]
        );

        $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY, 'dynamic_products_refresh', [
            'group' => 'General Information',
            'label' => '',
            'type' => 'int',
            'input' => '',
            'default' => 0,
            'note' => '',
            'backend' => '',
            'frontend' => '',
            'source' => '',
            'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_STORE,
            'visible' => false,
            'required' => false,
            'user_defined' => false,
            'visible_on_front' => false,
            'unique' => false
                ]
        );

        $setup->endSetup();
    }

}
