<?php

/**
 * Copyright © 2015 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade data for SImple Google Shopping
 */
class UpgradeData implements UpgradeDataInterface
{

    /** @var EavSetupFactory $eavSetupFactory */
    private $_eavSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(
    ModuleDataSetupInterface $setup,
            ModuleContextInterface $context
    )
    {

        if (version_compare($context->getVersion(), '3.0.2.3') < 0) {

            $setup->startSetup();

            /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->updateAttribute(
                    \Magento\Catalog\Api\Data\CategoryAttributeInterface::ENTITY_TYPE_CODE, 'dynamic_products_conds', 'is_global', '1'
            );
            $eavSetup->updateAttribute(
                    \Magento\Catalog\Api\Data\CategoryAttributeInterface::ENTITY_TYPE_CODE, 'dynamic_products_refresh', 'is_global', '1'
            );
            $setup->endSetup();
        }
    }

}
