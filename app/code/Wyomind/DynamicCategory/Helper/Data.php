<?php

/**
 * @category    Wyomind
 * @package     Wyomind_Dynamiccategory
 * @version     3.0.1.1
 * @copyright   Copyright (c) 2017 Wyomind (https://www.wyomind.com/)
 * @obfuscation
 * @ignore_var e
 */

namespace Wyomind\DynamicCategory\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var Wyomind_Licencemanager_Helper_Data
     */
    protected $_coreHelper;
    protected $_dynamicCatagoryRule;
    protected $_directoryList;
    protected $_indexerFactory;
    protected $_storeManager;
    protected $_categoryCollectionFactory;
    protected $_resourceModelReplaceParent;
    protected $_logger;
    protected $_unserialize;
    protected $_driverFileFactory;
    public $productUrlRewriteGenerator;
    public $urlRewriteFactory;
    public $urlRewriteCollectionFactory;
    public $urlPersist;
    public $error = "Dynamic Category: Invalid License!";

    public function __construct(
        \Magento\Framework\App\Helper\Context $context, \Wyomind\Core\Helper\Data $coreHelper,
        \Wyomind\DynamicCategory\Model\Rule $dynamicCatagoryRule,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Wyomind\DynamicCategory\Model\ResourceModel\ReplaceParent $resourceModelReplaceParent,
        \Wyomind\DynamicCategory\Logger\Logger $logger,
        \Magento\Framework\Unserialize\Unserialize $unserialize,
        \Magento\Framework\Filesystem\Driver\FileFactory $driverFileFactory,
        \Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        \Magento\UrlRewrite\Model\UrlPersistInterface $urlPersist
    )
    {


        $coreHelper->constructor($this, func_get_args());
        $this->_indexerFactory = $indexerFactory;
        $this->_directoryList = $directoryList;
        $this->_dynamicCatagoryRule = $dynamicCatagoryRule;
        $this->_coreHelper = $coreHelper;
        $this->_storeManager = $storeManager;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_resourceModelReplaceParent = $resourceModelReplaceParent;
        $this->_unserialize = $unserialize;
        $this->_driverFileFactory = $driverFileFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
        $this->urlPersist = $urlPersist;
        parent::__construct($context);
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Framework\DataObject $category
     * @return array
     */
    public function getDynamicProductIds(\Magento\Framework\DataObject $category)
    {
        if (!$category->getId()) {
            return [];
        }
        $ids = [];
        $conds = $category->getDynamicProductsConds();
        if (is_string($conds)) {
            $conds = $this->_unserialize->unserialize($conds);
        }

        $path = explode('/', $category->getPath());
        array_shift($path); // remove root category 1
        $rootCategory = array_shift($path);  // get root category (after 1/)

        $websites = [];
        foreach ($this->_storeManager->getStores() as $store) {
            if ($store->getRootCategoryId() == $rootCategory) {
                $websites[] = $store->getWebsiteId();
            }
        }

        if (!empty($conds)) {
            try {

                $this->_dynamicCatagoryRule->setWebsiteIds($websites);
                $this->_dynamicCatagoryRule->loadPost(['conditions' => $conds]);
                \Magento\Framework\Profiler::start('DYNAMIC CATEGORY MATCHING PRODUCTS');
                $productIds = $this->_dynamicCatagoryRule->getDynamicCategoryMatchingProductIds();
                \Magento\Framework\Profiler::stop('DYNAMIC CATEGORY MATCHING PRODUCTS');
                foreach ($productIds as $productId => $validationByWebsite) {
                    if (false !== array_search(1, $validationByWebsite, true)) {
                        $ids[] = $productId;
                    }
                }

                foreach ($conds as $cond) {
                    if (isset($cond['type']) && $cond['type'] == 'Wyomind\DynamicCategory\Model\Rule\Condition\Product\ReplaceParent') {
                        $strategy = $cond['value'];
                        $parentIds = $this->_resourceModelReplaceParent->getItems($ids);
                        if ($strategy == 'keep_orphans') {
                            $ids = array_unique(array_merge($parentIds, array_keys(array_diff_key(array_flip($ids), $parentIds))));
                        } else {
                            // Remove orphans
                            $ids = $parentIds;
                        }
                        break;
                    }
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                throw($e);
            }
        }
        return $ids;
    }

    /**
     * @param \Magento\Framework\DataObject $category
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getDynamicProductCollection(\Magento\Framework\DataObject $category)
    {
        $productIds = $this->getDynamicProductIds($category);
        $collection = $this->_dynamicCatagoryRule->getProductCollectionFactory()->create();
        if (!empty($productIds)) {
            $collection->addIdFilter($productIds);
        } else {
            $collection->addIdFilter([0]); // Workaround for empty collection
        }
        return $collection;
    }

    /**
     * @param \Magento\Framework\DataObject $category
     * @param $storeId
     * @return int
     */
    public function getCategoryProductCount(\Magento\Framework\DataObject $category, $storeId)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addIdFilter($category->getId())
            ->setProductStoreId($storeId)
            ->setLoadProductCount(true)
            ->setStoreId($storeId);
        return (int)$collection->getFirstItem()->getProductCount();
    }

    /**
     * @return bool
     */
    public function isLoggingEnabled()
    {
        return $this->_coreHelper->getStoreConfig('dynamiccategory/general/enable_reindex_log');
    }

}
