<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\DynamicCategory\Helper;

class Indexer extends \Wyomind\DynamicCategory\Helper\Data
{

    protected $_lockFile;
    protected $_isLocked;

    public function process(\Magento\Catalog\Model\Category $category)
    {
        if ($this->_isLocked($category)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The grid generation is being processed. Please try again later.'));
        }

        $this->_lock($category);
        $collection = $this->getDynamicProductCollection($category);
        $productIds = $collection->getAllIds();
        $oldProducts = $category->getProductsPosition();
        $products = array_fill_keys($productIds, '');
        $common = array_intersect_key($oldProducts, $products);
        $products = $common + $products;

        $category->setPostedProducts($products)
            ->setDynamicProductsRefresh(0)
            ->save();

        // generate new url rewrites
        foreach ($collection as $product) {
            foreach ($product->getStoreIds() as $storeId) {
                $this->urlPersist->deleteByData([
                    \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::ENTITY_ID => $product->getId(),
                    \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::ENTITY_TYPE => \Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator::ENTITY_TYPE,
                    \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::REDIRECT_TYPE => 0,
                    \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::STORE_ID => $storeId
                ]);
                try {
                    $this->urlPersist->replace($this->productUrlRewriteGenerator->generate($product));
                } catch (\Exception $e) {

                }
            }
        }

        $this->_unlock($category);
        if ($this->isLoggingEnabled()) {
            $this->_logger->notice(sprintf(
                '[Dynamic Category] Category %d successfully reindexed (%d): %s', $category->getId(), count($productIds), implode(', ', $productIds)
            ));
        }


        if ($this->_coreHelper->moduleIsEnabled("Wyomind_Elasticsearch")) {
            $this->_indexerFactory->create()->load('catalogsearch_fulltext')->invalidate();
        }
        $this->_indexerFactory->create()->load(\Magento\Catalog\Model\Indexer\Category\Product::INDEXER_ID)->invalidate();

        return $productIds;
    }

    /**
     * Get lock file resource
     *
     * @param \Magento\Framework\DataObject $category
     * @return resource
     */
    protected function _getLockFile(\Magento\Framework\DataObject $category)
    {
        if ($this->_lockFile === null) {
            $varDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::TMP);
            $file = $varDir . "/" . 'dynamiccategory_' . $category->getId() . '.lock';
            $driver = $this->_driverFileFactory->create();
            if ($driver->isExists($file)) {
                $this->_lockFile = $driver->fileOpen($file, 'w');
            } else {
                $this->_lockFile = $driver->fileOpen($file, 'x');
            }
            $driver->fileWrite($this->_lockFile, date('r'));
        }
        return $this->_lockFile;
    }

    /**
     * Lock process without blocking.
     * This method allow protect multiple process runing and fast lock validation.
     *
     * @param \Magento\Framework\DataObject $category
     * @return $this
     */
    protected function _lock(\Magento\Framework\DataObject $category)
    {
        $driver = $this->_driverFileFactory->create();
        $this->_isLocked = true;
        $driver->fileLock($this->_getLockFile($category), LOCK_EX | LOCK_NB);
        return $this;
    }

    /**
     * Unlock process
     *
     * @param \Magento\Framework\DataObject $category
     * @return $this
     */
    protected function _unlock(\Magento\Framework\DataObject $category)
    {
        $driver = $this->_driverFileFactory->create();
        $this->_isLocked = false;
        $driver->fileLock($this->_getLockFile($category), LOCK_UN);
        return $this;
    }

    /**
     * Check if process is locked
     *
     * @param \Magento\Framework\DataObject $category
     * @return bool
     */
    protected function _isLocked(\Magento\Framework\DataObject $category)
    {
        $driver = $this->_driverFileFactory->create();
        if ($this->_isLocked !== null) {
            return $this->_isLocked;
        }
        $fp = $this->_getLockFile($category);
        if ($driver->fileLock($fp, LOCK_EX | LOCK_NB)) {
            $driver->fileLock($fp, LOCK_UN);
            return false;
        }
        return true;
    }

    /**
     * Close file resource if it was opened
     */
    public function __destruct()
    {
        $driver = $this->_driverFileFactory->create();
        if ($this->_lockFile) {
            $driver->fileClose($this->_lockFile);
        }
    }

}
