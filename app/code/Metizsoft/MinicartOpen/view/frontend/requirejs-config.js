var config = {
    map: {
        '*': {
            minicart_open: 'Metizsoft_MinicartOpen/js/view/minicartopen'
        }
    },
    shim: {
        minicart_open: {
            deps: [
                'jquery'
            ]
        }
    }
};