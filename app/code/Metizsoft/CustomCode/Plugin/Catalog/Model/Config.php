<?php

namespace Metizsoft\CustomCode\Plugin\Catalog\Model;

class Config
{
    public function afterGetAttributeUsedForSortByArray(
    \Magento\Catalog\Model\Config $catalogConfig,
    $options
    ) {
        unset($options['price']);
        //$options['popularity'] = __('Popularity');
        $options['newness'] = __('Newness');
        $options['high_to_low'] = __('Price: High to Low');
        $options['low_to_high'] = __('Price: Low to High');
        
        return $options;

    }

}