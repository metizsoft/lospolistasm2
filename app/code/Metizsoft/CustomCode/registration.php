<?php
/**
 * Copyright © 2018 Porto. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Metizsoft_CustomCode',
    __DIR__
);
